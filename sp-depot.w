&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          gco              PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 
  Description: 
  Author: 
  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */


/* Local Variable Definitions ---                                       */
DEF VAR ii AS INT NO-UNDO.
DEF VAR logbid AS LOG NO-UNDO.

{hcombars.i}
{g-hprowinbase.i}

DEF VAR hcombars-f1 AS INT NO-UNDO.
DEF VAR FrmComBars-f1 AS HANDLE.

&SCOPED-DEFINE babandonner 101
&SCOPED-DEFINE bcreer 102
&SCOPED-DEFINE bmodifier 103
&SCOPED-DEFINE bsupprimer 104

{Hgcoergo.i}
{hfileopen.i}



    /* Parameters Definitions ---                                           */
/* DEFINE INPUT  PARAMETER pliste  AS CHARACTER    NO-UNDO.  */

    /* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME bw$depot

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tabgco

/* Definitions for BROWSE bw$depot                                      */
&Scoped-define FIELDS-IN-QUERY-bw$depot tabgco.n_tab tabgco.inti_tab tabgco.adresse[1] tabgco.adresse[2] tabgco.adresse[3] tabgco.ville tabgco.k_post tabgco.pays 
&Scoped-define ENABLED-FIELDS-IN-QUERY-bw$depot   
&Scoped-define SELF-NAME bw$depot
&Scoped-define QUERY-STRING-bw$depot FOR EACH tabgco WHERE tabgco.type_tab = "DS"
&Scoped-define OPEN-QUERY-bw$depot OPEN QUERY {&SELF-NAME} FOR EACH tabgco WHERE tabgco.type_tab = "DS".
&Scoped-define TABLES-IN-QUERY-bw$depot tabgco
&Scoped-define FIRST-TABLE-IN-QUERY-bw$depot tabgco


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-bw$depot}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bw$depot rs$search rs$depot rs$nom ~
rs$adresse RECT-320 RECT-362 
&Scoped-Define DISPLAYED-OBJECTS f$depot rs$search ft$titre1 f$nom ~
f$adresse rs$depot rs$nom rs$adresse 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE f$adresse AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 37 BY .79 NO-UNDO.

DEFINE VARIABLE f$depot AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 15 BY .79 NO-UNDO.

DEFINE VARIABLE f$nom AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 26 BY .79 NO-UNDO.

DEFINE VARIABLE ft$titre1 AS CHARACTER FORMAT "X(50)":U INITIAL "Gestion des d�pots" 
      VIEW-AS TEXT 
     SIZE 25.29 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE VARIABLE rs$adresse AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Commence par", 1,
"Contient", 2
     SIZE 37 BY .75 NO-UNDO.

DEFINE VARIABLE rs$depot AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "<", 1,
">", 2,
"=", 3
     SIZE 15 BY .75 NO-UNDO.

DEFINE VARIABLE rs$nom AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Commence par", 1,
"Contient", 2
     SIZE 26 BY .75 NO-UNDO.

DEFINE VARIABLE rs$search AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "N� DEPOT", 1,
"Nom", 2,
"Adresse", 3
     SIZE 92 BY .75 NO-UNDO.

DEFINE RECTANGLE RECT-320
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 157 BY 15.75.

DEFINE RECTANGLE RECT-362
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 27.14 BY 1.08.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY bw$depot FOR 
      tabgco SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE bw$depot
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS bw$depot C-Win _FREEFORM
  QUERY bw$depot DISPLAY
      tabgco.n_tab          FORMAT ">>>>>9"     COLUMN-LABEL "N� DEPOT"     WIDTH 15
      tabgco.inti_tab       FORMAT "X(30)"      COLUMN-LABEL "NOM DEPOT"    WIDTH 30
      tabgco.adresse[1]     FORMAT "X(35)"      COLUMN-LABEL "ADRESSE 1"    WIDTH 35
      tabgco.adresse[2]     FORMAT "X(35)"      COLUMN-LABEL "ADRESSE 2"    WIDTH 35
      tabgco.adresse[3]     FORMAT "X(35)"      COLUMN-LABEL "ADRESSE 3"    WIDTH 35
      tabgco.ville          FORMAT "X(30)"      COLUMN-LABEL "VILLE"        WIDTH 30
      tabgco.k_post         FORMAT ">>>>>9"     COLUMN-LABEL "CODE POSTAL"  WIDTH 30
      tabgco.pays           FORMAT "X(10)"      COLUMN-LABEL "PAYS"        WIDTH 30
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 155.14 BY 12
         FONT 49 ROW-HEIGHT-CHARS .54 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bw$depot AT ROW 6 COL 1.86 WIDGET-ID 200
     f$depot AT ROW 3.75 COL 28 COLON-ALIGNED NO-LABEL WIDGET-ID 44
     rs$search AT ROW 2.75 COL 30 NO-LABEL WIDGET-ID 40
     ft$titre1 AT ROW 2.17 COL 1.72 NO-LABEL WIDGET-ID 8
     f$nom AT ROW 3.75 COL 52 COLON-ALIGNED NO-LABEL WIDGET-ID 46
     f$adresse AT ROW 3.75 COL 81 COLON-ALIGNED NO-LABEL WIDGET-ID 48
     rs$depot AT ROW 4.75 COL 30 NO-LABEL WIDGET-ID 50
     rs$nom AT ROW 4.75 COL 54 NO-LABEL WIDGET-ID 54
     rs$adresse AT ROW 4.75 COL 83 NO-LABEL WIDGET-ID 58
     RECT-320 AT ROW 2.5 COL 1
     RECT-362 AT ROW 1.04 COL 1.14 WIDGET-ID 2
    WITH 1 DOWN NO-BOX OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 157.43 BY 17.33
         FONT 49 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Gestion des d�pots"
         COLUMN             = 5
         ROW                = 12
         HEIGHT             = 17.33
         WIDTH              = 157.43
         MAX-HEIGHT         = 320
         MAX-WIDTH          = 320
         VIRTUAL-HEIGHT     = 320
         VIRTUAL-WIDTH      = 320
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

&IF '{&WINDOW-SYSTEM}' NE 'TTY' &THEN
IF NOT C-Win:LOAD-ICON("progiwin.ico":U) THEN
    MESSAGE "Unable to load icon: progiwin.ico"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK.
&ENDIF
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB bw$depot 1 DEFAULT-FRAME */
/* SETTINGS FOR FILL-IN f$adresse IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$depot IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$nom IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN ft$titre1 IN FRAME DEFAULT-FRAME
   NO-ENABLE ALIGN-L                                                    */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE bw$depot
/* Query rebuild information for BROWSE bw$depot
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tabgco WHERE tabgco.type_tab = "DS".
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE bw$depot */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME DEFAULT-FRAME
/* Query rebuild information for FRAME DEFAULT-FRAME
     _Options          = "SHARE-LOCK KEEP-EMPTY"
     _Query            is NOT OPENED
*/  /* FRAME DEFAULT-FRAME */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Gestion des d�pots */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  RUN choose-babandonner.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME f$adresse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f$adresse C-Win
ON VALUE-CHANGED OF f$adresse IN FRAME DEFAULT-FRAME
DO:
  
  ASSIGN f$adresse rs$adresse.
  IF rs$adresse = 1 THEN 
      OPEN QUERY bw$depot FOR EACH tabgco WHERE tabgco.type_tab = "DS" AND (tabgco.adresse[1] MATCHES f$adresse + "*" OR tabgco.adresse[2] MATCHES f$adresse + "*" OR tabgco.adresse[3] MATCHES f$adresse + "*") NO-LOCK.
  ELSE IF rs$adresse = 2 THEN 
      OPEN QUERY bw$depot FOR EACH tabgco WHERE tabgco.type_tab = "DS" AND (tabgco.adresse[1] MATCHES "*" + f$adresse + "*" OR tabgco.adresse[2] MATCHES "*" + f$adresse + "*" OR tabgco.adresse[3] MATCHES "*" + f$adresse + "*") NO-LOCK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME f$depot
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f$depot C-Win
ON VALUE-CHANGED OF f$depot IN FRAME DEFAULT-FRAME
DO:
  ASSIGN f$depot rs$depot.
  IF rs$depot = 1 AND f$depot <> 0 THEN 
      OPEN QUERY bw$depot FOR EACH tabgco WHERE tabgco.type_tab = "DS" AND tabgco.n_tab < f$depot NO-LOCK.
  ELSE IF rs$depot = 2 AND f$depot <> 0 THEN 
      OPEN QUERY bw$depot FOR EACH tabgco WHERE tabgco.type_tab = "DS" AND tabgco.n_tab > f$depot NO-LOCK.
  ELSE IF rs$depot = 3 AND f$depot <> 0 THEN 
      OPEN QUERY bw$depot FOR EACH tabgco WHERE tabgco.type_tab = "DS" AND tabgco.n_tab = f$depot NO-LOCK.
  ELSE
      OPEN QUERY bw$depot FOR EACH tabgco WHERE tabgco.type_tab = "DS" NO-LOCK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME f$nom
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f$nom C-Win
ON VALUE-CHANGED OF f$nom IN FRAME DEFAULT-FRAME
DO:
  
  ASSIGN f$nom rs$nom.
  IF rs$nom = 1 THEN 
      OPEN QUERY bw$depot FOR EACH tabgco WHERE tabgco.type_tab = "DS" AND tabgco.inti_tab MATCHES f$nom + "*" NO-LOCK.
  ELSE IF rs$nom = 2 THEN 
      OPEN QUERY bw$depot FOR EACH tabgco WHERE tabgco.type_tab = "DS" AND tabgco.inti_tab MATCHES "*" + f$nom + "*" NO-LOCK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs$adresse
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs$adresse C-Win
ON VALUE-CHANGED OF rs$adresse IN FRAME DEFAULT-FRAME
DO:
  ASSIGN rs$adresse.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs$depot
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs$depot C-Win
ON VALUE-CHANGED OF rs$depot IN FRAME DEFAULT-FRAME
DO:
  ASSIGN rs$depot.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs$nom
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs$nom C-Win
ON VALUE-CHANGED OF rs$nom IN FRAME DEFAULT-FRAME
DO:
  ASSIGN rs$nom.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs$search
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs$search C-Win
ON VALUE-CHANGED OF rs$search IN FRAME DEFAULT-FRAME
DO:
    ASSIGN rs$search.

    DISABLE f$depot f$nom f$adresse WITH FRAME DEFAULT-FRAME.

    IF rs$search = 1 THEN
    DO:
        ENABLE f$depot WITH FRAME DEFAULT-FRAME IN WINDOW c-win.
    END.
    ELSE IF rs$search = 2 THEN
    DO:
        ENABLE f$nom WITH FRAME DEFAULT-FRAME IN WINDOW c-win.
    END.
    ELSE IF rs$search = 3 THEN
    DO:
        ENABLE f$adresse WITH FRAME DEFAULT-FRAME IN WINDOW c-win.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME bw$depot
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win C-Win


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */

/* Gestion des lignes/colonnes des fichiers interface */
ON WINDOW-CLOSE OF C-Win DO:
  RUN choose-babandonner.
  RETURN NO-APPLY.
END.

ON alt-cursor-up OF FRAME default-frame ANYWHERE DO:
    DEF VAR logbid AS LOG.
    logbid = CBFocusItem (hcombars-f1,2,{&babandonner}).
END.


ON CLOSE OF THIS-PROCEDURE DO:
    RUN putini.
    IF VALID-HANDLE(hComBars) THEN APPLY "close" TO hComBars.
    IF VALID-HANDLE (HndGcoErg) THEN APPLY "CLOSE" TO HndGcoErg.

    IF VALID-HANDLE (Hfileopen) THEN APPLY "close" TO hFileOpen.
    IF VALID-HANDLE(C-Win) THEN DELETE WIDGET C-Win.
    IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END.
  
ON RETURN OF c-win ANYWHERE DO:
    IF CAN-DO("fill-in,toggle-box,radio-set,combo-box",SELF:TYPE) THEN DO:
      APPLY "tab" TO SELF.
      RETURN NO-APPLY.
    END.
    ELSE IF SELF:TYPE="editor" THEN LOGBID=SELF:INSERT-STRING("~n").
    ELSE IF SELF:TYPE="button" THEN APPLY "choose" TO SELF.
    ELSE IF CAN-DO("browse,selection-list",SELF:TYPE) THEN APPLY "default-action" TO SELF.
END.


ON f6 OF FRAME default-frame ANYWHERE
    RUN choose-bcreer.

ON f8 OF FRAME default-frame ANYWHERE
    RUN choose-bmodifier.

ON LEAVE OF c-win ANYWHERE DO:
    IF SELF:bgcolor = 31 THEN SELF:bgcolor = 16.
END.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN proc-init.
  SESSION:SET-WAIT-STATE ("").
  VIEW c-win.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-babandonner C-Win 
PROCEDURE choose-babandonner :
DO WITH FRAME DEFAULT-FRAME : 

        APPLY "close" TO THIS-PROCEDURE.

    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bcreer C-Win 
PROCEDURE choose-bcreer :
DO WITH FRAME DEFAULT-FRAME : 
    RUN sp-saidepot(?,'C').
    OPEN QUERY bw$depot FOR EACH tabgco WHERE tabgco.type_tab = "DS" NO-LOCK.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bmodifier C-Win 
PROCEDURE choose-bmodifier :
DO WITH FRAME DEFAULT-FRAME :
    IF AVAIL tabgco THEN
        RUN sp-saidepot(tabgco.n_tab ,'M').
    OPEN QUERY bw$depot FOR EACH tabgco WHERE tabgco.type_tab = "DS" NO-LOCK.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bsupprimer C-Win 
PROCEDURE choose-bsupprimer :
DO WITH FRAME DEFAULT-FRAME : 
    DEFINE VARIABLE rep1 AS LOGICAL    NO-UNDO.

  MESSAGE "voulez vous supprimer !?"
      VIEW-AS ALERT-BOX INFO BUTTONS YES-NO UPDATE rep1.
  IF rep1 THEN
    DO:
      IF AVAIL tabgco THEN 
          DO:
          FIND CURRENT tabgco EXCLUSIVE-LOCK NO-ERROR.

               DELETE  tabgco.
          END.
  END.
  OPEN QUERY bw$depot FOR EACH tabgco WHERE tabgco.type_tab = "DS" NO-LOCK.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsCreation C-Win 
PROCEDURE CombarsCreation :
RUN gcoErgo PERSISTENT SET HndGcoErg ({&WINDOW-NAME}:HANDLE).
    SET-TITLE (FRAME DEFAULT-FRAME:HANDLE, {&WINDOW-NAME}:TITLE). 
    RUN combars PERSISTENT SET hcombars.
    hcombars-f1 = cbocxadd (FRAME DEFAULT-FRAME:HANDLE, OUTPUT FrmComBars-f1).
    CBSetSize(hcombars-f1, 1.04,  28.26, 1.15,FRAME DEFAULT-FRAME:WIDTH - 27.66).
    CBAddBar(hcombars-f1, 'Barre outils 1', {&xtpBarTop}, YES, NO).
    CBShowMenu(hcombars-f1, NO).
    CBToolBarAccelTips (hcombars-f1, NO).

    CBLoadIcon(hcombars-f1, {&babandonner},StdMedia + "\StdMedia.icl",1). 
    CBLoadIcon(hcombars-f1, {&bcreer},StdMedia + "\StdMedia.icl",36).
    CBLoadIcon(hcombars-f1, {&bmodifier},StdMedia + "\StdMedia.icl",78).
    CBLoadIcon(hcombars-f1, {&bsupprimer},StdMedia + "\StdMedia.icl",112). /*$99*/

    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&babandonner}, Translate ('&Abandonner') , YES).
    CBSetItemStyle(hcombars-f1, 2, {&babandonner}, {&xtpButtonIconAndCaption}).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bcreer}, Translate ('&Creer' + ' (F6)') , NO).
    CBSetItemStyle(hcombars-f1, 2, {&bcreer}, {&xtpButtonIconAndCaption}).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bmodifier}, Translate ('&Modifier' + ' (F8)') , NO).
    CBSetItemStyle(hcombars-f1, 2, {&bmodifier}, {&xtpButtonIconAndCaption}).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bsupprimer}, Translate ('&Supprimer') , NO).
    CBSetItemStyle(hcombars-f1, 2, {&bsupprimer}, {&xtpButtonIconAndCaption}).
        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsExecute C-Win 
PROCEDURE CombarsExecute :
DEF INPUT PARAM pnumcombar AS INT.
    DEF INPUT PARAM pbutton AS INT.

    IF pnumcombar = hcombars-f1 THEN DO:
        CASE pbutton :
            WHEN {&babandonner} THEN RUN choose-babandonner.
            WHEN {&bcreer} THEN RUN choose-bcreer.
            WHEN {&bmodifier} THEN RUN choose-bmodifier.
            WHEN {&bsupprimer} THEN RUN choose-bsupprimer.
        END CASE.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getini C-Win 
PROCEDURE getini :
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc-init C-Win 
PROCEDURE proc-init :
DO WITH FRAME default-frame:
     ENABLE ALL WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
     ft$titre1:SCREEN-VALUE = "Gestion des d�pots".
        RUN getini.

        {&OPEN-QUERY-{&BROWSE-NAME}}

        RUN combarscreation.

        APPLY "VALUE-CHANGED" TO rs$search.

    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE putini C-Win 
PROCEDURE putini :
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

