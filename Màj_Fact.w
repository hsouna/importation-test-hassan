&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          gco              PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR ii AS INT NO-UNDO.
DEF VAR logbid AS LOG NO-UNDO.

{hcombars.i}
{g-hprowinbase.i}

DEF VAR hcombars-f1 AS INT NO-UNDO.
DEF VAR FrmComBars-f1 AS HANDLE.

DEF TEMP-TABLE tps NO-UNDO:
FIELD cod_pro AS INTEGER .
FIELD nom_pro AS CHAR .
FIELD qte AS INT.

&SCOPED-DEFINE babandonner 101
&SCOPED-DEFINE bvalider 102

{Hgcoergo.i}
{hfileopen.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME BROWSE-2

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES histolig

/* Definitions for BROWSE BROWSE-2                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-2 histolig.cod_pro histolig.nom_pro ~
histolig.qte histolig.px_vte 
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-2 
&Scoped-define QUERY-STRING-BROWSE-2 FOR EACH histolig NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-BROWSE-2 OPEN QUERY BROWSE-2 FOR EACH histolig NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-BROWSE-2 histolig
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-2 histolig


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-BROWSE-2}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 f$t1 f$nompro f$pxvte f$nopro f$qte ~
BUTTON-1 BROWSE-2 
&Scoped-Define DISPLAYED-OBJECTS f$t1 f$t2 f$ad3 f$cde f$bl f$nofac ~
f$typmvt f$nompro f$pxvte f$nopro f$qte 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON BUTTON-1 
     LABEL "Valider" 
     SIZE 15 BY 1.13.

DEFINE VARIABLE f$ad3 AS CHARACTER FORMAT "X(256)":U 
     LABEL "Adresse" 
     VIEW-AS FILL-IN 
     SIZE 32.14 BY 1 NO-UNDO.

DEFINE VARIABLE f$bl AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "N� BL" 
     VIEW-AS FILL-IN 
     SIZE 9 BY 1 NO-UNDO.

DEFINE VARIABLE f$cde AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "N� Cde" 
     VIEW-AS FILL-IN 
     SIZE 9 BY 1 NO-UNDO.

DEFINE VARIABLE f$nofac AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "N� Facture" 
     VIEW-AS FILL-IN 
     SIZE 9 BY 1 NO-UNDO.

DEFINE VARIABLE f$nompro AS CHARACTER FORMAT "X(256)":U 
     LABEL "D�signation Produit" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE f$nopro AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "N� produit" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE VARIABLE f$pxvte AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "Prix de vente" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE f$qte AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Qte" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE f$t1 AS DATETIME FORMAT "99/99/99 HH:MM:SS":U 
     LABEL "Date" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE f$t2 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "N� Client" 
     VIEW-AS FILL-IN 
     SIZE 10.29 BY 1 NO-UNDO.

DEFINE VARIABLE f$typmvt AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type" 
     VIEW-AS FILL-IN 
     SIZE 7 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 132 BY 19.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-2 FOR 
      histolig SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-2 C-Win _STRUCTURED
  QUERY BROWSE-2 NO-LOCK DISPLAY
      histolig.cod_pro FORMAT ">>>>>>9":U WIDTH 12.43
      histolig.nom_pro FORMAT "X(35)":U
      histolig.qte FORMAT "->>>>>>9.99999":U
      histolig.px_vte FORMAT "->>>>>>>9.9999":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 131 BY 10.75 ROW-HEIGHT-CHARS .67 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     f$t1 AT ROW 3 COL 12 COLON-ALIGNED WIDGET-ID 2
     f$t2 AT ROW 3 COL 53.43 COLON-ALIGNED WIDGET-ID 4
     f$ad3 AT ROW 3 COL 80.29 COLON-ALIGNED WIDGET-ID 6
     f$cde AT ROW 5 COL 14 COLON-ALIGNED WIDGET-ID 8
     f$bl AT ROW 5 COL 36 COLON-ALIGNED WIDGET-ID 10
     f$nofac AT ROW 5 COL 65 COLON-ALIGNED WIDGET-ID 12
     f$typmvt AT ROW 5 COL 92 COLON-ALIGNED WIDGET-ID 14
     f$nompro AT ROW 7.96 COL 52.57 COLON-ALIGNED WIDGET-ID 18
     f$pxvte AT ROW 7.96 COL 99 COLON-ALIGNED WIDGET-ID 22
     f$nopro AT ROW 8 COL 19.72 COLON-ALIGNED WIDGET-ID 16
     f$qte AT ROW 8 COL 73.57 COLON-ALIGNED WIDGET-ID 20
     BUTTON-1 AT ROW 8 COL 121 WIDGET-ID 26
     BROWSE-2 AT ROW 9.25 COL 6 WIDGET-ID 200
     RECT-1 AT ROW 1 COL 6 WIDGET-ID 24
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 138 BY 19.96 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "<Gestion des factures>"
         HEIGHT             = 21.96
         WIDTH              = 138
         MAX-HEIGHT         = 21.96
         MAX-WIDTH          = 152.86
         VIRTUAL-HEIGHT     = 21.96
         VIRTUAL-WIDTH      = 152.86
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB BROWSE-2 BUTTON-1 DEFAULT-FRAME */
/* SETTINGS FOR FILL-IN f$ad3 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$bl IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$cde IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$nofac IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$t2 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$typmvt IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-2
/* Query rebuild information for BROWSE BROWSE-2
     _TblList          = "GCO.histolig"
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _FldNameList[1]   > GCO.histolig.cod_pro
"histolig.cod_pro" ? ? "integer" ? ? ? ? ? ? no ? no no "12.43" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _FldNameList[2]   = GCO.histolig.nom_pro
     _FldNameList[3]   = GCO.histolig.qte
     _FldNameList[4]   = GCO.histolig.px_vte
     _Query            is OPENED
*/  /* BROWSE BROWSE-2 */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* <Gestion des factures> */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* <Gestion des factures> */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 C-Win
ON CHOOSE OF BUTTON-1 IN FRAME DEFAULT-FRAME /* Valider */
DO:
      DEFINE VARIABLE rep AS LOGICAL    NO-UNDO.
    MESSAGE "Voulez vous valider ?"
        VIEW-AS ALERT-BOX INFO BUTTONS YES-NO UPDATE rep.
    IF rep THEN
    DO:
           .
            CREATE tps.
            ASSIGN
                tps.cod_pro     = f$nopro. 
                tps.nom_pro     = f$nompro  
                tps.qte         = f$qte
                tps.px_vte      = f$pxvte.
      
        END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME BROWSE-2
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */

/* Gestion des lignes/colonnes des fichiers interface */
ON WINDOW-CLOSE OF C-Win DO:
  RUN choose-babandonner.
  RETURN NO-APPLY.
END.

ON alt-cursor-up OF FRAME default-frame ANYWHERE DO:
    DEF VAR logbid AS LOG.
    logbid = CBFocusItem (hcombars-f1,2,{&babandonner}).
END.


ON CLOSE OF THIS-PROCEDURE DO:
    RUN putini.
    IF VALID-HANDLE(hComBars) THEN APPLY "close" TO hComBars.
    IF VALID-HANDLE (HndGcoErg) THEN APPLY "CLOSE" TO HndGcoErg.

    IF VALID-HANDLE (Hfileopen) THEN APPLY "close" TO hFileOpen.
    IF VALID-HANDLE(C-Win) THEN DELETE WIDGET C-Win.
    IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END.

ON f9 OF FRAME default-frame ANYWHERE
    RUN choose-bvalider.


ON LEAVE OF c-win ANYWHERE DO:
    IF SELF:bgcolor = 31 THEN SELF:bgcolor = 16.
END.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN proc-init.
  SESSION:SET-WAIT-STATE ("").
  VIEW c-win.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

    RUN proc-init.
  SESSION:SET-WAIT-STATE ("").
  VIEW c-win.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-babandonner C-Win 
PROCEDURE choose-babandonner :
DO WITH FRAME DEFAULT-FRAME : 

        APPLY "close" TO THIS-PROCEDURE.

    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bvalider C-Win 
PROCEDURE choose-bvalider :
DO WITH FRAME DEFAULT-FRAME : 

     FIND LAST histoent WHERE histoent.typ_mvt = "C"  EXCLUSIVE-LOCK NO-ERROR.
            IF AVAIL histoent THEN
            DO:
                CREATE histoent.
                DISPLAY  
                    f$t1:SCREEN-VALUE      = string(TODAY) 
                    f$t2:SCREEN-VALUE      = 
                    f$cde:SCREEN-VALUE     = STRING ( histoent.no_cde + 1 )
                    f$bl:SCREEN-VALUE      = STRING ( histoent.no_bl + 1 )
                    f$nofac:SCREEN-VALUE   = STRING ( histoent.no_fa_a[1] + 1 )
                    f$typmvt:SCREEN-VALUE  = "C" .
            END.
              DEFINE VARIABLE reps AS LOGICAL    NO-UNDO.
    
                 MESSAGE "VOULEZ VOUS Ajouter un Enregistrement ? "
                   VIEW-AS ALERT-BOX INFO BUTTONS YES-NO UPDATE reps.
              
            IF reps = YES THEN
            DO:
            CREATE histolig.
                FOR EACH histolig  NO-LOCK:
            
            ASSIGN  histolig.nompro = tps.
                    histolig.pxvte  =
                    histolig.nopro  =
                    histolig.qte    =.
            END.
           MESSAGE "LE TRAITEMENT TERMINE AVEC SUCCES !"
                    VIEW-AS ALERT-BOX INFO BUTTONS OK.
         
            END.
    DEFINE VARIABLE rep AS LOGICAL    NO-UNDO.
    MESSAGE "Voulez vous valider ?"
        VIEW-AS ALERT-BOX INFO BUTTONS YES-NO UPDATE rep.
    IF rep THEN
    DO:
           .
            CREATE tps.
            ASSIGN
                tps.cod_pro     =   .  
                tps.nom_pro     =   .   
                tps.qte         =   .
                tps.px_vte      =   .
                
            
        END.
        ELSE 
        DO:
            FIND FIRST histolig EXCLUSIVE-LOCK NO-ERROR.
            IF AVAIL histolig THEN
            DO:
                ASSIGN  
                    histolig.cod_pro      =   tps.cod_pro   
                    histolig.nom_pro      =   tps.nom_pro 
                    histolig.qte          =   tps.qte
                    histolig.px_vte       =   tps.px_vte.
            END.
        END.
        APPLY "close" TO THIS-PROCEDURE.
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsCreation C-Win 
PROCEDURE CombarsCreation :
RUN gcoErgo PERSISTENT SET HndGcoErg ({&WINDOW-NAME}:HANDLE).
    SET-TITLE (FRAME DEFAULT-FRAME:HANDLE, {&WINDOW-NAME}:TITLE). 
    RUN combars PERSISTENT SET hcombars.
    hcombars-f1 = cbocxadd (FRAME DEFAULT-FRAME:HANDLE, OUTPUT FrmComBars-f1).
    CBSetSize(hcombars-f1, 1.04,  28.26, 1.15,FRAME DEFAULT-FRAME:WIDTH - 27.66).
    CBAddBar(hcombars-f1, 'Barre outils 1', {&xtpBarTop}, YES, NO).
    CBShowMenu(hcombars-f1, NO).
    CBToolBarAccelTips (hcombars-f1, NO).

    CBLoadIcon(hcombars-f1, {&babandonner},StdMedia + "\StdMedia.icl",1). 
    CBLoadIcon(hcombars-f1, {&bvalider},StdMedia + "\StdMedia.icl",116).

    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&babandonner}, Translate ('&Abandonner') , YES).
    CBSetItemStyle(hcombars-f1, 2, {&babandonner}, {&xtpButtonIconAndCaption}).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bvalider}, Translate ('&Valider' + ' (F9)') , NO).
    CBSetItemStyle(hcombars-f1, 2, {&bvalider}, {&xtpButtonIconAndCaption}).
        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsExecute C-Win 
PROCEDURE CombarsExecute :
DEF INPUT PARAM pnumcombar AS INT.
    DEF INPUT PARAM pbutton AS INT.

    IF pnumcombar = hcombars-f1 THEN DO:
        CASE pbutton :
            WHEN {&babandonner} THEN RUN choose-babandonner.
            WHEN {&bvalider} THEN RUN choose-bvalider.
        END CASE.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY f$t1 f$t2 f$ad3 f$cde f$bl f$nofac f$typmvt f$nompro f$pxvte f$nopro 
          f$qte 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-1 f$t1 f$nompro f$pxvte f$nopro f$qte BUTTON-1 BROWSE-2 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

