/* INCLUDE:lignecl5.i... */
/* 
Auteur : HL
Include n� 5 commun aux sources utilisant CREATE LIGNECLI � partir du fichier produit
   Calcul montant HT 
$133 HL 29/04/99
$140 HL 06/05/99
$222 HL 17/08/99 fac_poi poi_var
$1100 DDG 04/11/05 Gestion du poids net � l'achat et � la vente
$1143 JL 13/02/06 Gestion pp_uv sur ligne Hors Stock
$1287 VB 14/05/07 Gestion de la 4i�me remise
$1798 AP 01/04/09 Gestion des remises en valeur
A55537 HL 4.5 24/04/13 Passage de 3 � 6 U.L, Explications dans {6ul.i}, Api {Api-UL.i}
*/

IF {1}.GRATUIT OR {1}.garantie or {1}.sous_type="PH" then {1}.mt_ht=0.
else do:
    /* Remises */
    /*$1798...
    if parsoc.typ_rem then 
        px-int={1}.px_vte - {1}.remise1 - {1}.remise2 - {1}.remise3 - {1}.remise4.
    else if {1}.sous_type<>"DF" then ASSIGN 
        PX-INT = {1}.px_vte - (({1}.px_vte * {1}.REMISE1) / 100) 
        PX-INT = PX-INT - ((PX-INT * {1}.REMISE2) / 100) 
        PX-INT = PX-INT - ((PX-INT * {1}.REMISE3) / 100)
        PX-INT = PX-INT - ((PX-INT * {1}.REMISE4) / 100).
    px-int=round(px-int,IF {1}.dev_vte<>g-dftdev AND AVAIL tabcomp THEN tabcomp.deb_ce ELSE g-ndv).
    */
     PX-INT = (IF {1}.num_colis = "" THEN {1}.px_vte ELSE {1}.px_refv).
     {calpx_rv.i parsoc.typ_rem PX-INT "NOARR" {1}.remise1 {1}.remise2 {1}.remise3 {1}.remise4 {1}.rem_val[1] {1}.rem_val[2] {1}.rem_val[3] {1}.rem_val[4]}

    px-int = round(px-int,IF {1}.dev_vte<>g-dftdev AND AVAIL tabcomp THEN tabcomp.deb_ce ELSE g-ndv).
    /*...$1798*/
    
    
    /* Montant HT ligne */
    if {1}.sous_type="PS" then
        {1}.mt_ht=round({1}.qte * px-int,IF {1}.dev_vte<>g-dftdev AND AVAIL tabcomp THEN tabcomp.deci_dev ELSE g-nbdec).
    else if {1}.sous_type<>"DF" then do:
        if {1}.poi_var then
            {1}.mt_ht=round(( /*$1100 ... */ (IF {1}.poid_net <> 0 THEN {1}.poid_net ELSE {1}.poid_bru) /* ... $1100 */ * px-int) / (if CAN-DO("AR,HS",{1}.sous_type) then {1}.pp_uv else 1),IF {1}.dev_vte<>g-dftdev AND AVAIL tabcomp THEN tabcomp.deci_dev ELSE g-nbdec).
        else if {1}.fac_poi then 
            {1}.mt_ht=round(({1}.qte * {1}.poid_bru * px-int) / (if CAN-DO("AR,HS",{1}.sous_type) then {1}.pp_uv else 1),IF {1}.dev_vte<>g-dftdev AND AVAIL tabcomp THEN tabcomp.deci_dev ELSE g-nbdec).
        else DO:
            wqv={1}.qte.
            IF {1}.num_colis <> "" THEN wqv = {1}.volume.
            ELSE IF {1}.sous_type="AR" AND {1}.uni_fac<>"" THEN
                wqv=ConversionStockFacturation("Q",wqv,{1}.uni_fac,{1}.cod_conv,{1}.k_var,{1}.cod_pro).
            {1}.mt_ht=round(px-int * UL_NbUE (wqv, {1}.uni_vte, {1}.nb_uv1, {1}.nb_uv2, {1}.nb_uv, {1}.nb_cv3, {1}.nb_cv)
                            / (if CAN-DO("AR,HS",{1}.sous_type) then {1}.pp_uv else 1),IF {1}.dev_vte<>g-dftdev AND AVAIL tabcomp THEN tabcomp.deci_dev ELSE g-nbdec).
        END.
    end.
    else if {1}.calcul=1 then
        {1}.mt_ht={1}.px_vte.
    else if {1}.calcul=2 then
        {1}.mt_ht=round(({1}.qte * {1}.px_vte) / (IF {1}.pp_uv=0 then 1 else {1}.pp_uv),IF {1}.dev_vte<>g-dftdev AND AVAIL tabcomp THEN tabcomp.deci_dev ELSE g-nbdec).

    /* Remises globales */
    IF ({1}.SOUS_TYPE <> "DF" OR {1}.CALCUL <= 2) and ({2}.rem_glo[1]<>0 or {2}.rem_glo[2]<>0) then do:
        IF {2}.rem_glo[1]<>0 then {1}.mt_ht={1}.mt_ht - (({1}.mt_ht * {2}.rem_glo[1]) / 100).
        IF {2}.rem_glo[2]<>0 then {1}.mt_ht={1}.mt_ht - (({1}.mt_ht * {2}.rem_glo[2]) / 100).
        {1}.mt_ht=ROUND({1}.mt_ht,IF {1}.dev_vte<>g-dftdev AND AVAIL tabcomp THEN tabcomp.deci_dev ELSE g-nbdec).
    END.
END.
/* ...INCLUDE:lignecl5.i */
