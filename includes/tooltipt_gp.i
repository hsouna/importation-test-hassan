/* Auteur : VR 
$A33567 MT 27/09/10 Infos pass�es aux fonctions*/

ON Ctrl-f9 OF {1} ANYWHERE
DO:
    DEF VAR v AS MEMPTR no-undo.
    DEF VAR ret AS INT no-undo.
    DEF VAR lbid AS LOG no-undo.
    DEF VAR ichamp AS INT NO-UNDO. /*$A33567*/

    IF FOCUS <> ? THEN
    DO:
        IF LOOKUP ("tooltip":U,LIST-QUERY-ATTRS (FOCUS)) <> 0 THEN /* on peut avoir tooltip */
        DO:
            IF NOT CAN-FIND (FIRST cleval where cleval.chapitre = "progiwin" and cleval.domaine = "GPAO" and cleval.cod_user = "SYSTEME":U and cleval.section = "MNTCST_GP" and cleval.cle = "OldTooltipFct":U NO-LOCK) 
                AND (FOCUS:TOOLTIP BEGINS Traduction("Fonction code",-2,"") + " " + ":" + "" + " " OR FOCUS:TOOLTIP BEGINS Traduction("Fonction grille",-2,"") + " " + ":" + "" + " " OR FOCUS:TOOLTIP BEGINS Traduction("Fonction tranche",-2,"") + " " + ":" + "" + " " OR FOCUS:TOOLTIP BEGINS Traduction("Fonction restriction",-2,"") + " " + ":" + "" + " ") THEN
            DO:
                /*$A33567...*/
                IF FOCUS:PRIVATE-DATA <> "" THEN 
                DO:
                    ichamp = LOOKUP("CHAMP":U,FOCUS:PRIVATE-DATA,"|").
                    IF ichamp > 0 THEN SetSessionData("GPAO","P%resfct_gp%nom_champ",ENTRY(ichamp + 1,FOCUS:PRIVATE-DATA,"|")).
                    ichamp = LOOKUP("CONTEXTE":U,FOCUS:PRIVATE-DATA,"|").
                    IF ichamp > 0 THEN SetSessionData("GPAO","P%resfct_gp%contexte",ENTRY(ichamp + 1,FOCUS:PRIVATE-DATA,"|")).
                END.
                /*...$A33567*/
                RUN visfctg_gp (FOCUS:TOOLTIP).
                /*$A33567...*/
                SetSessionData("GPAO","P%resfct_gp%nom_champ","").
                SetSessionData("GPAO","P%resfct_gp%contexte","").
                /*...$A33567*/
            END.
            ELSE
            DO:
                SET-SIZE (v) = 8.
                RUN GetCursorPos IN g-hprowin (GET-POINTER-VALUE (v), OUTPUT ret).
                IF (ret <> 0) THEN
                DO:
                    RUN TOOLTIP_gp PERSISTENT (?,?,?,?,?,?,?,?, ?, GET-LONG (v,1),GET-LONG (v,5),?,{1}:TITLE,FOCUS:TOOLTIP,"ooooooooo":U, FOCUS, OUTPUT lbid, {1}).
                    IF lbid THEN
                    DO:
                        APPLY "entry" TO FOCUS.
                        RETURN NO-APPLY.
                    END.
                END.                
                SET-SIZE (v) = 0.            
            END.
        END.
    END.
END.
