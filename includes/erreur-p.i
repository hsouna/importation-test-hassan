/* Envoi message erreur */
if txtmsg<>"" then do:
    message txtmsg view-as alert-box error title current-window:title IN WINDOW CURRENT-WINDOW.
    txtmsg="".
    IF VALID-HANDLE(wid) THEN apply "entry" to wid.
    return error.
end.

