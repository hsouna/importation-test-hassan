/* HL : fonction jour f�ri� pour FRANCE 

$OPT PPE 26/10/09 Utilisation d'une table temporaire pour stocker le jour de paques par ann�e 
                  et ainsi �viter de relancer le pgm "paques" � chaque fois 
                  (0.02 sec de chargement par "run paques" )
*/
FIND parvs WHERE parvs.ndos=g-ndos NO-LOCK NO-ERROR.
DEFINE VARIABLE lgVPaques AS LOGICAL    NO-UNDO.
IF NOT AVAIL parvs OR parvs.pays="" OR parvs.pays="FRA":U THEN lgVPaques = YES.
/*$OPT...*/
&IF DEFINED (tt_jour_paque) = 0 &THEN /* si &scoped-define non d�fini dans le source initial */
DEFINE TEMP-TABLE tt_jour_paque NO-UNDO 
    FIELD annee AS INT 
    FIELD jour_de_paque AS DATE 
INDEX jpaq annee.
&ENDIF
/*...$OPT*/
FUNCTION ferie RETURNS LOGICAL ( INPUT dat AS DATE) :
&IF DEFINED (VPAQUES) = 0 &THEN /* si &scoped-define non d�fini dans le source initial */
    DEFINE VARIABLE vpaques AS DATE NO-UNDO.
    IF lgVPaques THEN DO:
    /*$OPT...*/
    FIND FIRST tt_jour_paque WHERE tt_jour_paque.annee = YEAR(dat) NO-LOCK NO-ERROR .
    IF AVAILABLE tt_jour_paque THEN vpaques = tt_jour_paque.jour_de_paque .
    ELSE DO : 
    /*...$OPT*/
        RUN paques (string(YEAR(dat)), OUTPUT vpaques).
    /*$OPT...*/
            CREATE tt_jour_paque .
            ASSIGN tt_jour_paque.annee = YEAR(dat) tt_jour_paque.jour_de_paque = vpaques .
    END.
    /*...$OPT*/
    
&ENDIF
    IF dat = date("01/01/" + string(YEAR(dat))) 
        OR dat = date("01/05/" + string(YEAR(dat))) 
        OR dat = date("08/05/" + string(YEAR(dat))) 
        OR dat = date("14/07/" + string(YEAR(dat)))
        OR dat = date("15/08/" + string(YEAR(dat)))
        OR dat = DATE("01/11/" + string(YEAR(dat)))
        OR dat = DATE("11/11/" + string(YEAR(dat)))
        OR dat = DATE("25/12/" + string(YEAR(dat)))
        OR dat = vpaques + 1
        OR dat = vpaques + 39
        /*OR dat = vpaques + 50*/ THEN RETURN TRUE.
        ELSE RETURN FALSE.
&IF DEFINED (VPAQUES) = 0 &THEN /* si &scoped-define non d�fini dans le source initial */
    END.
    ELSE RETURN FALSE.
&ENDIF
END FUNCTION.
