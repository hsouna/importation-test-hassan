/* Auteur : HL */
DEF SHARED VARIABLE g-nbdec AS INT. /* Nb décimales montants */
DEF VAR fmtm AS CHAR no-undo.
fmtm=(IF g-nbdec = 0 THEN "9" ELSE "9." + FILL("9",g-nbdec)).
