/* Envoi message erreur */
if txtmsg<>"" then do:
    message txtmsg view-as alert-box error title current-window:title IN WINDOW CURRENT-WINDOW.
    txtmsg="".
    return no-apply.
end.
