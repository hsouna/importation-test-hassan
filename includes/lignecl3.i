/* 
Auteur : HL
Include n� 3 commun aux sources utilisant CREATE LIGNECLI � partir du fichier produit
   Init valeurs libres
$133 HL 29/04/1999
$560 HL 20/04/00 chemin
$A61779 CGU 05/06/13 Gestion d'une unit� sp�cifique
*/

&IF "{&Opsys}" = "WIN32" &THEN
&GLOBAL-DEFINE Slash ~\
&ELSE
&GLOBAL-DEFINE Slash ~/
&ENDIF

/* Initialisations valeurs libres (� ne pas utiliser si cr�ation apr�s trt �cran) */
FOR EACH TABLIB WHERE TABLIB.TYP_FICH="P" AND tablib.CDE_cli=YES AND AV-cli>0 NO-LOCK:
    if substr(cod_libr,1,1)="A" then val-ini={1}.zal[int(substr(cod_libr,2,1))].
    if substr(cod_libr,1,1)="D" then val-ini=string({1}.zda[int(substr(cod_libr,2,1))]).
    if substr(cod_libr,1,1)="N" then val-ini=string({1}.znu[int(substr(cod_libr,2,1))]).
    if substr(cod_libr,1,1)="T" then val-ini={1}.zta[int(substr(cod_libr,2,1))].
    if substr(cod_libr,1,1)="L" then val-ini=string({1}.zlo[int(substr(cod_libr,2,1))]).

    /* Programme initialisation  */
    find pgm where pgm.no_pgm=tablib.av-cli no-lock no-error.
    if available pgm then do:
      vchar = TrouveRepertoire(pgm.no_pgm). /*$A61779*/
      if search(vchar)<>? then run value(vchar) ({1}.cod_pro,{1}.cod_dec1,{1}.cod_dec2,{1}.cod_dec3,{1}.cod_dec4,{1}.cod_dec5,input-output val-ini).
    end.

    if substr(cod_libr,1,1)="A" then {1}.zal[int(substr(cod_libr,2,1))]=val-ini.
    if substr(cod_libr,1,1)="D" then {1}.zda[int(substr(cod_libr,2,1))]=date(val-ini).
    if substr(cod_libr,1,1)="N" then {1}.znu[int(substr(cod_libr,2,1))]=decimal(val-ini).
    if substr(cod_libr,1,1)="T" then {1}.zta[int(substr(cod_libr,2,1))]=val-ini.
    if substr(cod_libr,1,1)="L" then {1}.zlo[int(substr(cod_libr,2,1))]=(val-ini="yes").
END.
