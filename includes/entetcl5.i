/* 
Auteur : HL
Include n� 5 commun aux sources utilisant CREATE ENTETCLI � partir du fichier client
Plan �ch�ances

$133 HL 29/04/1999
$A55544 JL 05/07/13 Compta Multi-soci�t�
*/
X = IF {1}.CL_PAYE <> 0 THEN {1}.CL_PAYE
    ELSE IF {1}.CL_FACT <> 0 THEN {1}.CL_FACT
    ELSE {1}.COD_CLI.

for each plechcli where plechcli.ndos = (IF g-multisoc THEN g-ndos ELSE 0)  AND /*$A55544*/ plechcli.cod_cli=X no-lock:
    create echcli.
    ASSIGN 
        echcli.cod_cli  = {1}.cod_cli /* Toujours le code client de la pi�ce */
        echcli.typ_sai  = {1}.typ_sai
        echcli.no_cde   = {1}.no_cde
        echcli.no_bl    = {1}.no_bl
        echcli.code_reg = plechcli.code_reg
        echcli.nb_jour  = plechcli.nb_jour
        echcli.pourc    = plechcli.pourc.
    {1}.nb_ech = {1}.nb_ech + 1.
end.
