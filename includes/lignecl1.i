/* 
Auteur : HL
Include n� 1 commun aux sources utilisant CREATE LIGNECLI � partir du fichier produit

$133 HL 29/04/1999
$1070 HL 18/08/05 Multi-Soci�t� (Rechercher : parvs, n_d, parsoc.mult_soc)
*/

ASSIGN
    {1}.cod_cli    = {2}.cod_cli
    {1}.typ_sai    = {2}.typ_sai
    {1}.no_cde     = {2}.no_cde
    {1}.no_bl      = {2}.no_bl
    {1}.depot      = {2}.depot
    {1}.ndos       = {2}.ndos
    {1}.dat_liv    = {2}.dat_liv
    {1}.dat_livd   = {2}.dat_livd
    {1}.dat_acc    = {2}.dat_acc
    {1}.dat_rll    = {2}.dat_rll
    {1}.commerc[1] = {2}.commerc[1]
    {1}.commerc[2] = {2}.commerc[2]
    {1}.app_aff    = {2}.app_aff.
