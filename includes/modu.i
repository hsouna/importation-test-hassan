/* Cr�� par VR 
      Modulo avec des d�cimaux */
      
FUNCTION DecModulo RETURNS DEC (INPUT a AS DEC, INPUT b AS DEC) :
    DEF VAR tmp AS DEC no-undo.

    tmp = TRUNC (a / b,0).
    tmp = a - (tmp * b).
    RETURN tmp.
END function.
