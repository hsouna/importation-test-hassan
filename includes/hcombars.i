DEF VAR hComBars AS HANDLE NO-UNDO.

/* Style d'affichages des CommandBars */
&GLOB xtpThemeOffice2000  0
&GLOB xtpThemeOfficeXP    1
&GLOB xtpThemeOffice2003  2
&GLOB xtpThemeNativeWinXP 3
&GLOB xtpThemeWhidbey     4
&GLOB xtpThemeOffice2007  5
&GLOB xtpThemeRibbon      6

/* Type de bouttons d'une CommandBar */
&GLOB xtpControlError            0
&GLOB xtpControlButton           1
&GLOB xtpControlPopup            2
&GLOB xtpControlButtonPopup      3
&GLOB xtpControlSplitButtonPopup 4
&GLOB xtpControlComboBox         5
&GLOB xtpControlEdit             6
&GLOB xtpControlCustom           7
&GLOB xtpControlLabel            8
&GLOB xtpControlCheckBox         9
&GLOB xtpControlGallery          10
&GLOB xtpControlRadioButton      11

/* Position d'une CommandBar */
&GLOB xtpBarTop    0
&GLOB xtpBarBottom 1
&GLOB xtpBarLeft   2
&GLOB xtpBarRight  3

/* Style d'un boutton */
&GLOB xtpButtonAutomatic             0
&GLOB xtpButtonCaption               1
&GLOB xtpButtonIcon                  2
&GLOB xtpButtonIconAndCaption        3
&GLOB xtpButtonIconAndCaptionBelow   4
&GLOB xtpButtonCaptionAndDescription 5

/* Option des tooltips */
&GLOB xtpToolTipStandard 0
&GLOB xtpToolTipBalloon  1
&GLOB xtpToolTipOffice   2
&GLOB xtpToolTipLuna     4

&GLOB xtpToolTipIconNone    0
&GLOB xtpToolTipIconInfo    1
&GLOB xtpToolTipIconWarning 2
&GLOB xtpToolTipIconError   3

/* Options pour les ShortCut de menu (acc�l�rateurs) */
&GLOB vk_Shift 4
&GLOB vk_Ctrl  8
&GLOB vk_Alt   16

&GLOB vk_F1  112
&GLOB vk_F2  113
&GLOB vk_F3  114
&GLOB vk_F4  115
&GLOB vk_F5  116
&GLOB vk_F6  117
&GLOB vk_F7  118
&GLOB vk_F8  119
&GLOB vk_F9  120
&GLOB vk_F10 121
&GLOB vk_F11 122
&GLOB vk_F12 123

&GLOB vk_Return    13
&GLOB vk_Suppr     46
&GLOB vk_Space     32
&GLOB vk_BackSpace 8
&GLOB vk_Tab       9
&GLOB vk_Echap     27

&GLOB vk_PageUp    33
&GLOB vk_PageDown  34
&GLOB vk_End       35
&GLOB vk_Home      36
&GLOB vk_Left      37
&GLOB vk_Up        38
&GLOB vk_Right     39
&GLOB vk_Down      40
&GLOB vk_Insert    45
&GLOB vk_Multiply  106
&GLOB vk_Add       107
&GLOB vk_Substract 109
&GLOB vk_Decimal   110
&GLOB vk_Divide    111

/* Styles de couleur */
&GLOB STDCOLOR_BACKGROUND 1
&GLOB STDCOLOR_ACTIVECAPTION 2
&GLOB STDCOLOR_INACTIVECAPTION 3
&GLOB STDCOLOR_MENU 4
&GLOB STDCOLOR_WINDOW 5
&GLOB STDCOLOR_WINDOWFRAME 6
&GLOB STDCOLOR_MENUTEXT 7
&GLOB STDCOLOR_WINDOWTEXT 8
&GLOB STDCOLOR_CAPTIONTEXT 9
&GLOB STDCOLOR_ACTIVEBORDER 10
&GLOB STDCOLOR_INACTIVEBORDER 11
&GLOB STDCOLOR_APPWORKSPACE 12
&GLOB STDCOLOR_HIGHLIGHT 13
&GLOB STDCOLOR_HIGHLIGHTTEXT 14
&GLOB STDCOLOR_BTNFACE 15
&GLOB STDCOLOR_BTNSHADOW 16
&GLOB STDCOLOR_GRAYTEXT 17
&GLOB STDCOLOR_BTNTEXT 18
&GLOB STDCOLOR_INACTIVECAPTIONTEXT 19
&GLOB STDCOLOR_BTNHIGHLIGHT 20
&GLOB STDCOLOR_3DDKSHADOW 21
&GLOB STDCOLOR_3DLIGHT 22
&GLOB STDCOLOR_INFOTEXT 23
&GLOB STDCOLOR_INFOBK 24
&GLOB STDCOLOR_HOTLIGHT 26
&GLOB STDCOLOR_GRADIENTACTIVECAPTION 27
&GLOB STDCOLOR_GRADIENTINACTIVECAPTION 28
&GLOB XPCOLOR_TOOLBAR_FACE 30
&GLOB XPCOLOR_HIGHLIGHT 31
&GLOB XPCOLOR_HIGHLIGHT_BORDER 32
&GLOB XPCOLOR_HIGHLIGHT_PUSHED 33
&GLOB XPCOLOR_HIGHLIGHT_CHECKED 36
&GLOB XPCOLOR_HIGHLIGHT_CHECKED_BORDER 37
&GLOB XPCOLOR_ICONSHADDOW 34
&GLOB XPCOLOR_GRAYTEXT 35
&GLOB XPCOLOR_TOOLBAR_GRIPPER 38
&GLOB XPCOLOR_SEPARATOR 39
&GLOB XPCOLOR_DISABLED 40
&GLOB XPCOLOR_MENUBAR_FACE 41
&GLOB XPCOLOR_MENUBAR_EXPANDED 42
&GLOB XPCOLOR_MENUBAR_BORDER 43
&GLOB XPCOLOR_MENUBAR_TEXT 44
&GLOB XPCOLOR_HIGHLIGHT_TEXT 45
&GLOB XPCOLOR_TOOLBAR_TEXT 46
&GLOB XPCOLOR_PUSHED_TEXT 47
&GLOB XPCOLOR_TAB_INACTIVE_BACK 48
&GLOB XPCOLOR_TAB_INACTIVE_TEXT 49
&GLOB XPCOLOR_HIGHLIGHT_PUSHED_BORDER 50
&GLOB XPCOLOR_3DFACE 51
&GLOB XPCOLOR_3DSHADOW 52
&GLOB XPCOLOR_FRAME 54
&GLOB XPCOLOR_SPLITTER_FACE 55
&GLOB XPCOLOR_LABEL 56
&GLOB XPCOLOR_STATICFRAME 57
&GLOB WIDCOLOR_BG_LIGHT 65
&GLOB WIDCOLOR_BG_DARK  66

/* Option pour l'�dition dans les ComboBox ou les Edit */
&GLOB xtpEditStyleLeft      0
&GLOB xtpEditStyleCenter    1
&GLOB xtpEditStyleRight     2
&GLOB xtpEditStyleLowerCase 16
&GLOB xtpEditStylePassword  32

/* Apparence des onglets */
&GLOB xtpTabAppearancePropertyPage         0
&GLOB xtpTabAppearancePropertyPageSelected 1
&GLOB xtpTabAppearancePropertyPageFlat     2
&GLOB xtpTabAppearancePropertyPage2003     3
&GLOB xtpTabAppearanceStateButtons         4
&GLOB xtpTabAppearanceVisualStudio         5
&GLOB xtpTabAppearanceFlat                 6
&GLOB xtpTabAppearanceExcel                7
&GLOB xtpTabAppearanceVisio                8
&GLOB xtpTabAppearanceVisualStudio2005     9
&GLOB xtpTabAppearancePropertyPage2007     10

/* Style de bordure de la zone client des onglets */
&GLOB xtpTabFrameBorder     0
&GLOB xtpTabFrameSingleLine 1
&GLOB xtpTabFrameNone       2

/* Style de couleurs des onglets */
&GLOB xtpTabColorDefault      1
&GLOB xtpTabColorVisualStudio 2
&GLOB xtpTabColorOffice2003   4
&GLOB xtpTabColorWinXP        8
&GLOB xtpTabColorWhidbey      16
&GLOB xtpTabColorOffice2007   32

/* Style des onglets */
&GLOB xtpTabLayoutAutoSize   0
&GLOB xtpTabLayoutSizeToFit  1
&GLOB xtpTabLayoutFixed      2
&GLOB xtpTabLayoutCompressed 3
&GLOB xtpTabLayoutMultiRow   4
&GLOB xtpTabLayoutRotated    5


FUNCTION GetContexteLancement RETURNS CHAR   IN hComBars.
FUNCTION GetHandleLancement   RETURNS HANDLE IN hComBars.

FUNCTION RGB RETURNS INT (pRed AS INT, pGreen AS INT, pBlue AS INT) IN hComBars.

FUNCTION CBDesactiverLesFocus RETURNS LOG () IN hComBars.
FUNCTION CBActiverLesFocus    RETURNS LOG () IN hComBars.

/* Fonctions relatives � l'objet 'CommandBars' */
FUNCTION CBOCXAdd    RETURNS INT (pFrame AS HANDLE, OUTPUT pFrmOCX AS HANDLE) IN hComBars.
FUNCTION CBOCXDelete RETURNS LOG (pNum AS INT) IN hComBars.

FUNCTION CBestPersonnalisee RETURNS LOGICAL (pprog AS CHAR, pbarre AS CHAR) IN hComBars. /* $GC */

FUNCTION CBGetParentFrame RETURNS HANDLE (pNum AS INT) IN hComBars.

FUNCTION CBSetStyle  RETURNS LOG (pNum AS INT, pStyle AS INT) IN hComBars.

FUNCTION CBRedraw RETURNS LOG (pNum AS INT, pNumItem AS INT) IN hComBars.

FUNCTION CBSetColor 	   RETURNS LOG (pNum AS INT, pType AS INT, pColor AS INT) IN hComBars.
FUNCTION CBGetColor 	   RETURNS INT (pNum AS INT, pType AS INT) 		  IN hComBars.
FUNCTION CBGetWindowsColor RETURNS INT (pNum AS INT, pType AS INT) 		  IN hComBars.

FUNCTION CBLoadBars  RETURNS LOG (pNum AS INT, pChemin AS CHAR) IN hComBars.

FUNCTION CBLostFocus RETURNS LOG (pNum AS INT) IN hComBars.

FUNCTION CBSetSize       RETURNS LOG (pNum AS INT, pRow AS DEC, pCol AS DEC, pHeight AS DEC, pWidth AS DEC) IN hComBars.
FUNCTION CBSetIconSize   RETURNS LOG (pNum AS INT, pWidth AS INT, pHeight AS INT)                           IN hComBars.
FUNCTION CBSetIconSizeEx RETURNS LOG (pNum AS INT, pLargeIcons AS LOG, pWidth AS INT, pHeight AS INT)       IN hComBars.

FUNCTION CBSetFont RETURNS LOG (pNum AS INT, pName AS CHAR, pSize AS INT, pBold AS LOG, pItalic AS LOG, pStrikeThrough AS LOG, pUnderline AS LOG) IN hComBars.

FUNCTION CBGetLastClick   RETURNS INT (pNum AS INT) IN hComBars.
FUNCTION CBResetLastClick RETURNS LOG (pNum AS INT) IN hComBars.

FUNCTION CBSavePlace RETURNS LOG (pNum AS INT) IN hComBars.
FUNCTION CBLoadPlace RETURNS LOG (pNum AS INT) IN hComBars.

FUNCTION CBSetDescriptionBtn RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, comment AS CHAR) IN hComBars. /* $GC */
FUNCTION CBGetDescriptionBtn RETURNS CHAR (pNum AS INT, pNumBar AS INT, pNumItem AS INT)  IN hComBars. /* $GC */

/* Fonctions relatives � l'objet 'CommandBar' (une barre de l'OCX CommandBars) */
FUNCTION CBGetCountBars  RETURNS INT (pNum AS INT) IN hComBars.
FUNCTION CBGetPathImage     RETURNS CHAR (pNum AS INT, iconId AS INT) IN hComBars. /* $GC */
FUNCTION CBGetPicture RETURN COM-HANDLE (pTagImage AS CHAR) IN hCombars. /* $GC */

FUNCTION CBRemoveAllBars RETURNS LOG (pNum AS INT, pRemoveMenuBar AS LOG) IN hComBars.
FUNCTION CBRemoveBar     RETURNS LOG (pNum AS INT, pNumBar AS INT) IN hComBars.

FUNCTION CBBarSetIconSize RETURNS LOG (pNum AS INT, pNumBar AS INT, pWidth AS INT, pHeight AS INT) IN hComBars.

FUNCTION CBAddBar        RETURNS INT (pNum AS INT, pName AS CHAR, pPosition AS INT, pStreched AS LOG, pMenuPopup AS LOG) IN hComBars.

FUNCTION CBShowMenu         RETURNS LOG (pNum AS INT, pShow AS LOG) IN hComBars.
FUNCTION CBShowBar          RETURNS LOG (pNum AS INT, pNumBar AS INT, pShow AS LOG) IN hComBars.
FUNCTION CBShowGripper      RETURNS LOG (pNum AS INT, pNumBar AS INT, pShow AS LOG) IN hComBars.

/* Fonctions relatives � l'objet 'ImageManagerIcons' (liste des images utilis�es dans l'OCX) */
FUNCTION CBAddImageEx      RETURNS LOG (pNum AS INT, pNumImage AS INT, pPath AS CHAR, pWidth AS INT)           IN hComBars.
FUNCTION CBAddImage        RETURNS LOG (pNum AS INT, pNumImage AS INT, pPath AS CHAR)                          IN hComBars.
FUNCTION CBRemoveAllImages RETURNS LOG (pNum AS INT)                                                           IN hComBars.
FUNCTION CBRemoveImage     RETURNS LOG (pNum AS INT, pNumImage AS INT)                                         IN hComBars.
FUNCTION CBGetCountImages  RETURNS INT (pNum AS INT)                                  	                       IN hComBars.
FUNCTION CBLoadImageList   RETURNS LOG (pNum AS INT, pPathWrx AS CHAR, pNameImageList AS CHAR)                 IN hComBars.
FUNCTION CBLoadMyImageList RETURNS LOG (pNum AS INT, pImageList AS COM-HANDLE)       		                   IN hComBars.
FUNCTION CBLoadIcon        RETURNS LOG (pNum AS INT, pNumImage AS INT, pPath AS CHAR, pIconIndexInFile AS INT) IN hComBars.
FUNCTION CBLoadIconEx      RETURNS LOG (pNum AS INT, pNumImage AS INT, pPath AS CHAR, pIconIndexInFile AS INT, pWidth AS INT) IN hComBars.

/* Fonctions relatives � l'objet 'CommandBarControls' (liste des boutons d'une CommandBar) */
FUNCTION CBRemoveAllItems RETURNS LOG (pNum AS INT, pNumBar AS INT)                  IN hComBars.
FUNCTION CBRemoveItem     RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT) IN hComBars.
FUNCTION CBRemoveSubItems RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT) IN hComBars.

FUNCTION CBGetItemComHandle RETURNS COM-HANDLE (pNum AS INT, pNumBar AS INT, pNumItem AS INT) IN hComBars.
FUNCTION CBGetItemHandle    RETURNS HANDLE     (pNum AS INT, pNumBar AS INT, pNumItem AS INT) IN hComBars.

FUNCTION CBAddItem        RETURNS COM-HANDLE (pNum AS INT, pNumBar AS INT, pParent AS COM-HANDLE, pType AS INT, pNumItem AS INT, pCaption AS CHAR, pBeginGroup AS LOG)    IN hComBars.
FUNCTION CBAddItemPrint   RETURNS COM-HANDLE (pNum AS INT, pNumBar AS INT, pParent AS COM-HANDLE, pNumItem AS INT, pCaption AS CHAR, pBeginGroup AS LOG, pNomPgm AS CHAR) IN hComBars.
FUNCTION CBGetCountItems     RETURNS INT (pNum AS INT, pNumBar AS INT, pParent AS COM-HANDLE)            IN hComBars. /* $GC */
FUNCTION CBGetIdItem         RETURNS INT (pNum AS INT, pNumBar AS INT, pParent AS COM-HANDLE, pNumItem AS INT) IN hComBars. /* $GC */
FUNCTION CBGetBeginGroupItem RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT) IN hComBars. /* $GC */
FUNCTION CBGetTypeItem       RETURNS INT (pNum AS INT, pNumBar AS INT, pNumItem AS INT) IN hComBars. /* GC */
FUNCTION CBGetItem           RETURNS COM-HANDLE(pNum AS INT, pNumBar AS INT, pParent AS COM-HANDLE, pNumItem AS INT) IN hComBars. /* $GC */
FUNCTION CBNouveauxBoutons   RETURNS CHAR (prog AS CHAR, nomb AS CHAR, pNum AS INT, pNumBar AS INT)  IN hComBars. /* $GC */

FUNCTION CBSetItemEdition RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pNomPgm AS CHAR) IN hComBars.

/* Fonctions relatives � l'objet 'CommandBarControl' (un item d'une CommandBar) */
FUNCTION CBSetItemStyle RETURNS LOG  (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pStyle AS INT) IN hComBars.
FUNCTION CBGetItemStyle RETURNS INT (pNum AS INT, pNumBar AS INT, pNumItem AS INT)                 IN hComBars.
FUNCTION CBSetItemData  RETURNS LOG  (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pData AS CHAR) IN hComBars.
FUNCTION CBGetItemData  RETURNS CHAR (pNum AS INT, pNumBar AS INT, pNumItem AS INT)                IN hComBars.
FUNCTION CBEnableItem   RETURNS LOG  (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pState AS LOG) IN hComBars.
FUNCTION CBShowItem     RETURNS LOG  (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pState AS LOG) IN hComBars.
FUNCTION CBCheckItem    RETURNS LOG  (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pState AS LOG) IN hComBars.
FUNCTION CBFocusItem    RETURNS LOG  (pNum AS INT, pNumBar AS INT, pNumItem AS INT)                IN hComBars.
FUNCTION CBResetItem 	RETURNS LOG  (pNum AS INT, pNumBar AS INT, pNumItem AS INT)                IN hComBars.

FUNCTION CBSetItemBeginGroup RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pBeginGroup AS LOG) IN hComBars.

FUNCTION CBExecuteItem RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT) IN hComBars.

FUNCTION CBGetItemEnableState RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT) IN hComBars.
FUNCTION CBGetItemShowState   RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT) IN hComBars.
FUNCTION CBGetItemCheckState  RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT) IN hComBars.

FUNCTION CBGetItemState RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, OUTPUT pVisible AS LOG, OUTPUT pEnable AS LOG, OUTPUT pCheck AS LOG) IN hComBars.
FUNCTION CBSetItemState RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pVisible AS LOG, pEnable AS LOG, pCheck AS LOG)                      IN hComBars.

FUNCTION CBSetItemRightAlign RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pRightAlign AS LOG) IN hComBars.
FUNCTION CBGetItemRightAlign RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT)                     IN hComBars.
FUNCTION CBSetItemLeftPopup  RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pLeftPopup AS LOG)  IN hComBars.

FUNCTION CBGetItemCaption     RETURNS CHAR (pNum AS INT, pNumBar AS INT, pNumItem AS INT)                      IN hComBars.
FUNCTION CBGetItemCaptionNC RETURNS CHAR (pNum AS INT, pNumBar AS INT, pNumItem AS INT)                        IN hComBars. /* $GC */
FUNCTION CBSetItemCaption     RETURNS LOG  (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pCaption AS CHAR)    IN hComBars.
FUNCTION CBSetItemTooltip     RETURNS LOG  (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pTooltip AS CHAR)    IN hComBars.
FUNCTION CBAddItemPerso	      RETURNS COM-HANDLE (pNum AS INT, pNumBar AS INT)				       IN hComBars.
FUNCTION CBSetItemDescription RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pDescription AS CHAR) IN hComBars.
FUNCTION CBSetItemIcon        RETURNS LOG  (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pIcon AS INT)        IN hComBars.
FUNCTION CBGetItemIcon 	      RETURNS INT  (pNum AS INT, pNumBar AS INT, pNumItem AS INT)			           IN hComBars.
FUNCTION CBSetItemWidth       RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pWidth AS INT)        IN hComBars.
FUNCTION CBSetItemHeight      RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pHeight AS INT)       IN hComBars.

FUNCTION CBSetPopupBar RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumControl AS INT, pPopup AS LOG, pWidth AS INT) IN hComBars.

FUNCTION CBSetItemDoubleGripper RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pShow AS LOG) IN hComBars.

/* Fonctions relatives � l'objet 'CommandBarEdit' (un item d'une CommandBar de style 'Edit') */
FUNCTION CBEditSetText RETURNS LOG  (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pText AS CHAR) IN hComBars.
FUNCTION CBEditGetText RETURNS CHAR (pNum AS INT, pNumBar AS INT, pNumItem AS INT)                IN hComBars.

FUNCTION CBEditSetReadOnly          RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pReadOnly AS LOG)          IN hComBars.
FUNCTION CBEditSetHint              RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pHint AS CHAR)             IN hComBars.
FUNCTION CBEditShowLabel            RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pShow AS LOG)              IN hComBars.
FUNCTION CBEditSetWidth             RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pWidth AS INT)             IN hComBars.
FUNCTION CBEditShellAutoComplete    RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pShellAutoComplete AS LOG) IN hComBars.
FUNCTION CBEditSetEditStyle         RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pEditStyle AS INT)         IN hComBars.
FUNCTION CBEditShowSpinButtons      RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pShow AS LOG)              IN hComBars.

/* Fonctions relatives � l'objet 'CommandBarComboBox' (un item d'une CommandBar de style 'ComboBox') */
FUNCTION CBComboAddItem           RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pText AS CHAR, pData AS INT)                IN hComBars.
FUNCTION CBComboAddItemEx         RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pText AS CHAR, pData AS INT, pIndex AS INT) IN hComBars.
FUNCTION CBComboAddList           RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pText AS CHAR)                              IN hComBars.
FUNCTION CBComboAddListPairs      RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pText AS CHAR)                              IN hComBars.
FUNCTION CBComboAddListEx         RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pText AS CHAR, pSeparator AS CHAR)          IN hComBars.
FUNCTION CBComboAddListPairsEx    RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pText AS CHAR, pSeparator AS CHAR)          IN hComBars.
FUNCTION CBComboRemoveItem        RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pText AS CHAR)                              IN hComBars.
FUNCTION CBComboRemoveItemEx      RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pIndex AS INT)                              IN hComBars.
FUNCTION CBComboGetListCount      RETURNS INT (pNum AS INT, pNumBar AS INT, pNumItem AS INT)                                             IN hComBars.
FUNCTION CBComboClear             RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT)                                             IN hComBars.
FUNCTION CBComboSetSel            RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pText AS CHAR)                              IN hComBars.
FUNCTION CBComboSetSelEx          RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pIndex AS INT)                              IN hComBars.
FUNCTION CBComboGetSel            RETURNS INT (pNum AS INT, pNumBar AS INT, pNumItem AS INT, OUTPUT pText AS CHAR)                       IN hComBars.
FUNCTION CBComboGetSelEx          RETURNS INT (pNum AS INT, pNumBar AS INT, pNumItem AS INT, OUTPUT pText AS CHAR)                       IN hComBars.
FUNCTION CBComboCanEdit           RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pCanEdit AS LOG)                            IN hComBars.
FUNCTION CBComboSetDropDownWidth  RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pWidth AS INT)                              IN hComBars.
FUNCTION CBComboSetWidth          RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pWidth AS INT)                              IN hComBars.
FUNCTION CBComboSetHint           RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pHint AS CHAR)                              IN hComBars.
FUNCTION CBComboSetIcon           RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pIcon AS INT)                               IN hComBars.
FUNCTION CBComboAutoComplete      RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pAutoComplete AS LOG)                       IN hComBars.
FUNCTION CBComboShellAutoComplete RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pShellAutoComplete AS LOG)                  IN hComBars.
FUNCTION CBComboSetEditStyle      RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pEditStyle AS INT)                          IN hComBars.

/* Fonctions pour des menus 'popup' */
FUNCTION CBShowMenuPopup RETURNS INT (pNum AS INT, pNumBar AS INT) IN hComBars.

/* Fonctions pour les acc�l�rateurs de menus */
FUNCTION CBAddShortCutEx     RETURNS LOG (pNum AS INT, pShiftCtrlAlt AS INT, pKey AS INT, pNumItem AS INT)  IN hComBars.
FUNCTION CBAddShortCut       RETURNS LOG (pNum AS INT, pShiftCtrlAlt AS INT, pKey AS CHAR, pNumItem AS INT) IN hComBars.
FUNCTION CBDeleteAllShortCut RETURNS LOG (pNum AS INT)                                                      IN hComBars.

/* Fonctions relatives � l'objet 'CommandBarGalleryItems' (une belle gallerie ...) */
FUNCTION CBGalleryAddLabel      RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pLabel AS CHAR)                             IN hComBars.
FUNCTION CBGalleryAddItem       RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pIdNewItem AS INT, pCaptionNewItem AS CHAR) IN hComBars.
FUNCTION CBGalleryRemoveAll     RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT)                                             IN hComBars.
FUNCTION CBGallerySetSizeItems  RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pHeight AS INT, pWidth AS INT)              IN hComBars.
FUNCTION CBGalleryShowScrollBar RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pShow AS LOG)                               IN hComBars.

/* Fonctions relatives � l'objet 'CommandBarsOptions' */
FUNCTION CBShowTextBelowIcons   RETURNS LOG (pNum AS INT, pShow AS LOG) IN hComBars.
FUNCTION CBShowPopupBarToolTips RETURNS LOG (pNum AS INT, pShow AS LOG) IN hComBars.
FUNCTION CBToolBarAccelTips     RETURNS LOG (pNum AS INT, pShow AS LOG) IN hComBars.
FUNCTION CBToolBarScreenTips    RETURNS LOG (pNum AS INT, pShow AS LOG) IN hComBars.

/* Fonctions relatives � l'objet 'TooltipContext' */
FUNCTION CBTooltipSetStyle RETURNS LOG (pNum AS INT, pStyle AS INT, pIcon AS INT) IN hComBars.

/* Fonctions relatives aux 'onglets' */
FUNCTION CBAddBarOnglet      RETURNS INT (pNum AS INT, pName AS CHAR)                                              IN hComBars.
FUNCTION CBBarOngletSetWidth RETURNS LOG (pNum AS INT, pNumBar AS INT, pWidth AS INT)                              IN hComBars.
FUNCTION CBAddOnglet         RETURNS LOG (pNum AS INT, pNumBar AS INT, pName AS CHAR, pIndex AS INT, pIcon AS INT) IN hComBars.

FUNCTION CBBarOngletSetStyle  RETURNS LOG (pNum AS INT, pNumBar AS INT, pStyle AS INT, pBoldSelected AS LOG, pBorderStyle AS INT, pHotTrack AS LOG, pLayoutStyle AS INT) IN hComBars.
FUNCTION CBBarOngletSetColors RETURNS LOG (pNum AS INT, pNumBar AS INT, pColorStyle AS INT, pUseLunaColors AS LOG, pOneColorByTab AS LOG)                                IN hComBars.

FUNCTION CBBarOngletShowIcons   RETURNS LOG (pNum AS INT, pNumBar AS INT, pShow AS LOG)                          IN hComBars.
FUNCTION CBBarOngletSetIconSize RETURNS LOG (pNum AS INT, pNumBar AS INT, pWidth AS INT, pHeight AS INT)         IN hComBars.

FUNCTION CBDeleteOnglet RETURNS LOG (pNum AS INT, pNumBar AS INT, pIndex AS INT) IN hComBars.

FUNCTION CBBarOngletSetSelection RETURNS LOG (pNum AS INT, pNumBar AS INT, pIndex AS INT) IN hComBars.
FUNCTION CBBarOngletGetSelection RETURNS INT (pNum AS INT, pNumBar AS INT)                IN hComBars.

FUNCTION CBBarOngletGetListeOnglets RETURNS CHAR (pNum AS INT, pNumBar AS INT) IN hComBars.

FUNCTION CBBarOngletGetOngletCount RETURNS INT (pNum AS INT, pNumBar AS INT) IN hComBars.

FUNCTION CBBarOngletMoveOnglet RETURNS LOG (pNum AS INT, pNumBar AS INT, pIndex AS INT, pNewIndex AS INT) IN hComBars.

FUNCTION CBBarOngletSetOngletCaption RETURNS LOG  (pNum AS INT, pNumBar AS INT, pIndex AS INT, pCaption AS CHAR) IN hComBars.
FUNCTION CBBarOngletGetOngletCaption RETURNS CHAR (pNum AS INT, pNumBar AS INT, pIndex AS INT)                   IN hComBars.

FUNCTION CBBarOngletSetOngletTooltip RETURNS LOG (pNum AS INT, pNumBar AS INT, pIndex AS INT, pTooltip AS CHAR) IN hComBars.
FUNCTION CBBarOngletSetOngletColor   RETURNS LOG (pNum AS INT, pNumBar AS INT, pIndex AS INT, pColor AS INT)    IN hComBars.
FUNCTION CBBarOngletGetOngletEnable  RETURNS LOG (pNum AS INT, pNumBar AS INT, pIndex AS INT)                   IN hComBars.
FUNCTION CBBarOngletSetOngletEnable  RETURNS LOG (pNum AS INT, pNumBar AS INT, pIndex AS INT, pEnable AS LOG)   IN hComBars.
FUNCTION CBBarOngletGetOngletVisible RETURNS LOG (pNum AS INT, pNumBar AS INT, pIndex AS INT)                   IN hComBars.
FUNCTION CBBarOngletSetOngletVisible RETURNS LOG (pNum AS INT, pNumBar AS INT, pIndex AS INT, pShow AS LOG)     IN hComBars.

FUNCTION CBBarOngletSetOngletIcon RETURNS LOG (pNum AS INT, pNumItem AS INT, pIndex AS INT, pIcon AS INT) IN hComBars.

FUNCTION CBSetItemOnglet RETURNS LOG (pNum AS INT, pNumBar AS INT, pNumItem AS INT, pOnglet AS CHAR) IN hComBars.
