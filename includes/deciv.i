/* Auteur : HL 
    $A77998 CGU 27/05/14 Optimisation pour Miko
*/
/********************************************************************************
      !       ATTENTION LES ELEMENTS SUIVANTS POINTENT SUR DES TABLES TEMPORAIRES
     ! !         - enttab
    !   !        - tabgco
   !  !  !       - fctdiv
  !   !   !      - typdec
 !    !    !     - tabcomp
!!!!!!!!!!!!!    - parvs $A77998
                 - pards $A77998
              SI UTILISER A PARTIR  restor2.p
*********************************************************************************/
DEF SHARED VARIABLE g-ndv   AS INT. /* Nb décimales prix vente */
DEF VAR fmtv AS CHAR no-undo.
fmtv=(IF g-ndv = 0 THEN "9" ELSE "9." + FILL("9",g-ndv)).
