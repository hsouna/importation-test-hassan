/*--------------------------------------------------------------------------------------------------
  Radical.i
  Include des proc�dures/d�finition pour la gestion des radicaux de comptes

    {1} : "modulo"   pour utilisation longueur code tiers et modulo
  ou
    {1} : Nom du radical ("401")

$1289 JL 24/04/07 CPTEQUIV passe de la base COMPTA � POCWIN
-------------------------------------------------------------------------------------------------*/
  
&IF DEFINED(g-cptequiv) = 0 &THEN
    &GLOBAL-DEFINE g-cptequiv g-cptequiv
    DEF VAR g-cptequiv-ngr1 AS INT no-undo.
    DEF VAR g-cptequiv-ngr2 AS INT no-undo.
    DEF VAR g-cptequiv-ndos AS INT no-undo.
    assign g-cptequiv-ngr1  = GetSessionInt("PocwinShared", "g-cptequiv-ngr1":U, 0)
           g-cptequiv-ngr2  = GetSessionInt("PocwinShared", "g-cptequiv-ngr2":U, 0) 
           g-cptequiv-ndos  = GetSessionInt("PocwinShared", "g-cptequiv-ndos":U, 0).
&ENDIF
  
&IF DEFINED(def-radical-{1}) = 0 &THEN
  
&GLOBAL-DEFINE def-radical-{1} def-radical-{1}
    
&IF "{1}" = "modulo":U &THEN
  
  DEF VAR radical-modulo AS INT NO-UNDO INIT 1000000. /* Utilisation modulo et compte de tiers */

  FOR FIRST cptequiv WHERE cptequiv.ngr1 = g-cptequiv-ngr1
                       and cptequiv.ngr2 = g-cptequiv-ngr2
                       and cptequiv.ndos = g-cptequiv-ndos
                       AND cptequiv.radic_franc = 411 NO-LOCK:
      CASE LENGTH(STRING(Cptequiv.radic_expl)):
          WHEN 2 THEN radical-modulo = 10000000.
          WHEN 4 THEN radical-modulo = 100000.
          WHEN 5 THEN radical-modulo = 10000.
      END CASE.
  END.

  DEF VAR radical-service AS CHAR NO-UNDO INIT "". 
  FOR EACH cptequiv WHERE cptequiv.ngr1=g-cptequiv-ngr1
                      and cptequiv.ngr2=g-cptequiv-ngr2
                      and cptequiv.ndos=g-cptequiv-ndos
                      and cptequiv.radic_franc = 9998 NO-LOCK:
      radical-service = radical-service + STRING(Cptequiv.radic_expl) + "*,".
  END.
  IF radical-service = "" THEN radical-service = "604*,611*,613*,615*,616*,621*,622*,624*,706*,7082*,7084*".
  ELSE radical-service = RIGHT-TRIM(radical-service,",").

&ELSE

  DEF VAR radical-{1} AS CHAR NO-UNDO INIT "{1}*".     /* Radical des comptes exploit�s - france */

  FOR FIRST cptequiv WHERE cptequiv.ngr1 = g-cptequiv-ngr1
                       and cptequiv.ngr2 = g-cptequiv-ngr2
                       and cptequiv.ndos = g-cptequiv-ndos /*AND cptequiv.radic_expl <> INT({1})*/ 
                       AND cptequiv.radic_franc = INT({1}) NO-LOCK:
      radical-{1} = STRING(cptequiv.radic_expl) + "*".
  END.

&ENDIF

&ENDIF
