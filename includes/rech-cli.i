/* Author: HL
   Recherche d'un client 
   
   $1155 JL 08/03/06 Interdire un type en recherche
   SC 20/03/09 Procedure CLI-RECHINIT
   $A69159 AFR 07/01/14 Recherche optimis�e client par contient
*/
DEFINE VARIABLE lgCliRechInit   AS LOGICAL    NO-UNDO INIT NO.
DEFINE VARIABLE zztypeCliRech as CHARACTER NO-UNDO.
DEFINE VARIABLE zzphoneCliRech AS CHARACTER NO-UNDO.

/* $A69159... */
DEF VAR rechac-actif-cli AS INT    NO-UNDO. /*  0=> non actif,>0 => taille d�composition*/
DEF VAR rechac-actif-cli-des AS INT    NO-UNDO. /*  0=> non actif,>0 => taille d�composition*/
DEF VAR rechac-actif-cli-motcle AS INT    NO-UNDO. /*  0=> non actif,>0 => taille d�composition*/
DEF VAR rechac-actif-cli-ville AS INT    NO-UNDO. /*  0=> non actif,>0 => taille d�composition*/
DEF VAR ind-i-cli AS INT NO-UNDO.
DEF VAR vcod_rech_cli AS INT    NO-UNDO.
DEF BUFFER Client-rech FOR Client.
DEFINE VARIABLE wTypFich AS CHARACTER  NO-UNDO.
wTypFich = "C".
/* ...$A69159 */

/*SC 20/03/09...*/
PROCEDURE CLI-RECHINIT:
    IF LoadSettings("RECCLI", OUTPUT hsettingsapi) THEN DO:
        ASSIGN 
            zztypeCliRech = GetSetting("Type", ?)
            zzphoneCliRech = GetSetting("Phonetique", ?).
        UnloadSettings(hSettingsApi).
    END.
    lgCliRechInit = YES.
END PROCEDURE.
/*...SC 20/03/09*/


PROCEDURE rech-cli:
    DEFINE INPUT   PARAM zzcde    AS CHARACTER NO-UNDO.  /* Commande "","C" */
    DEFINE INPUT   PARAM zzstatut AS CHARACTER NO-UNDO.  /* Liste Statuts � omettre */
    DEFINE INPUT-O PARAM zzcpt    AS CHARACTER NO-UNDO.  /* N� ou d�but Nom          */
    DEFINE OUTPUT  PARAM zzinti   AS CHARACTER NO-UNDO.  /* Intitul� en retour       */

    def var zzvnum as INTEGER NO-UNDO.
    def var zzpcpt as CHARACTER NO-UNDO.
    def var zzpinti as CHARACTER NO-UNDO.
    def var zzok as LOGICAL NO-UNDO.
    
    DEF VAR zzvchar AS CHARACTER NO-UNDO.
    DEF VAR zzvchar2 AS CHARACTER NO-UNDO.
    DEF VAR pas-droit-type AS LOG NO-UNDO.

    IF NOT lgCliRechInit THEN RUN CLI-RECHINIT.

    assign 
        zzok=no 
        zzvnum=integer(zzcpt) no-error.

    /* $A69159...*/
    ASSIGN rechac-actif-cli = -1 rechac-actif-cli-des = -1 rechac-actif-cli-motcle = -1 rechac-actif-cli-ville = -1
           vcod_rech_cli = ?.
    /* ...$A69159 */

    if error-status:error OR SUBSTR(zzcpt,1,1)=" " then do:
        /* Init champs recherche : " " = contains zzvchar, "&" = contains zzvchar2 */
        IF SUBSTR(zzcpt,1,1)=" " THEN DO:
            IF INDEX(zzcpt,'&')<>0 THEN ASSIGN 
                zzvchar=SUBSTR(zzcpt,2,INDEX(zzcpt,'&') - 2)
                zzvchar2=SUBSTR(zzcpt,INDEX(zzcpt,'&') + 1).
            ELSE ASSIGN 
                zzvchar=SUBSTR(zzcpt,2) 
                zzvchar2="".
        END.
        
        /*$A69159...*/
        ASSIGN
            vcod_rech_cli = 0.
        IF zzphoneCliRech="yes" THEN DO:
            zzvchar=IF SUBSTR(zzcpt,1,1)=" " THEN DoSoundex(zzvchar) ELSE DoSoundex(zzcpt).
            IF zzvchar2<>"" THEN zzvchar2=DoSoundex(zzvchar2).
        END.

        IF rechac-actif-cli-des = -1 THEN DO:
          FIND FIRST rechap WHERE rechap.typ_fich = wTypFich AND rechap.typ_cha = "D|A":U NO-LOCK NO-ERROR.
          IF AVAIL rechap THEN
              rechac-actif-cli-des = rechap.decompo.      
        END.
        IF rechac-actif-cli-motcle = -1 THEN DO:
            FIND FIRST rechap WHERE rechap.typ_fich = wTypFich AND rechap.typ_cha = "M|A":U NO-LOCK NO-ERROR.
            IF AVAIL rechap THEN
                rechac-actif-cli-motcle = rechap.decompo.
        END.
        IF rechac-actif-cli-ville = -1 THEN DO:
            FIND FIRST rechap WHERE rechap.typ_fich = wTypFich AND rechap.typ_cha = "V|A":U NO-LOCK NO-ERROR.
            IF AVAIL rechap THEN
                rechac-actif-cli-ville = rechap.decompo.
        END.
        /*...$A69159*/

        IF zztypeCliRech=? THEN DO:
            /*$A69159...*/
            IF rechac-actif-cli-des > 0 AND SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN DO:
                FIND FIRST rechac WHERE rechac.typ_fich = wTypFich AND 
                                      rechac.typ_cha = "D" AND 
                                      rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-cli-des) AND 
                                      rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK NO-ERROR.
                IF AVAIL rechac THEN DO: 
                    ASSIGN vcod_rech_cli = rechac.cod_tiers.
                    find CLIENTR where clientr.cod_cli = vcod_rech_cli no-lock no-error.
                END.
            END.
            ELSE /*...$A69159*/ IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN 
                find CLIENTR where clientr.nom_cli > "" AND CLIENTR.nom_cli MATCHES "*" + zzvchar + "*" no-lock no-error.
            ELSE /*$A69159...*/
            IF rechac-actif-cli-des > 0 AND SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN DO:
                FIND FIRST rechac WHERE rechac.typ_fich = wTypFich AND 
                                      rechac.typ_cha = "D" AND 
                                      rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-cli-des) AND 
                                      rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                      rechac.valeur MATCHES "*" + zzvchar2 + "*" NO-LOCK NO-ERROR.
                IF AVAIL rechac THEN DO: 
                    ASSIGN vcod_rech_cli = rechac.cod_tiers.
                    find CLIENTR where clientr.cod_cli = vcod_rech_cli no-lock no-error.
                END.
            END.
            ELSE /*...$A69159*/ IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN 
                find CLIENTR where clientr.nom_cli > "" AND CLIENTR.nom_cli MATCHES "*" + zzvchar + "*" AND CLIENTR.nom_cli MATCHES "*" + zzvchar2 + "*" no-lock no-error.
            ELSE /*$A69159...*/
            IF rechac-actif-cli-motcle > 0 THEN DO:
                FIND FIRST rechac WHERE rechac.typ_fich = wTypFich AND 
                                      rechac.typ_cha = "M" AND 
                                      rechac.decompo = SUBSTRING(zzcpt,1,rechac-actif-cli-motcle) AND 
                                      rechac.valeur MATCHES "*" + zzcpt + "*" NO-LOCK NO-ERROR.
                IF AVAIL rechac THEN DO: 
                    ASSIGN vcod_rech_cli = rechac.cod_tiers.
                    find CLIENTR where clientr.cod_cli = vcod_rech_cli no-lock no-error.
                END.
            END.
            ELSE DO: /*...$A69159*/ 
                find CLIENTR where CLIENTR.mot_cle=zzcpt no-lock no-error.
            END. /*$A69159*/
            if not available clientr then 
                find CLIENTR where CLIENTR.nom_cli begins zzcpt no-lock no-error.
            if zzphoneCliRech="yes" AND not available clientr then DO:
                IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN 
                    find clientr where clientr.phone > "" and clientr.phone MATCHES "*" + zzvchar + "*" no-lock no-error.
                ELSE IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN 
                    find clientr where clientr.phone > "" and clientr.phone MATCHES "*" + zzvchar + "*" AND clientr.phone MATCHES "*" + zzvchar2 + "*"  no-lock no-error.
                ELSE 
                    find clientr where clientr.phone begins zzvchar no-lock no-error.
            END.
        END.
        ELSE DO:
            /*$A69159...*/
            IF rechac-actif-cli-des > 0 AND SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN DO:
                FOR EACH rechac WHERE rechac.typ_fich = wTypFich AND 
                                      rechac.typ_cha = "D" AND 
                                      rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-cli-des) AND 
                                      rechac.valeur MATCHES "*" + zzvchar + "*" NO-LOCK,
                                    FIRST client-rech WHERE client-rech.cod_cli = rechac.cod_tiers
                                                        AND client.typ_elem = zztypeCliRech
                                                        AND client-rech.nom_cli MATCHES "*" + zzvchar + "*" NO-LOCK:
                    ASSIGN vcod_rech_cli = rechac.cod_tiers.
                    find CLIENTR where clientr.cod_cli = vcod_rech_cli no-lock no-error.
                END.
            END.
            ELSE /*...$A69159*/ IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN 
                find CLIENTR where clientr.typ_elem=zztypeCliRech AND clientr.nom_cli > "" AND CLIENTR.nom_cli MATCHES "*" + zzvchar + "*" no-lock no-error.
            ELSE /*$A69159...*/ IF rechac-actif-cli-des > 0 AND SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN DO:
                FOR EACH rechac WHERE rechac.typ_fich = wTypFich AND 
                                      rechac.typ_cha = "D" AND 
                                      rechac.decompo = SUBSTRING(zzvchar,1,rechac-actif-cli-des) AND 
                                      rechac.valeur MATCHES "*" + zzvchar + "*" AND
                                      rechac.valeur MATCHES "*" + zzvchar2 + "*",
                                FIRST client-rech WHERE client-rech.cod_cli = rechac.cod_tiers
                                                    AND client.typ_elem = zztypeCliRech
                                                    AND client-rech.nom_cli MATCHES "*" + zzvchar + "*" 
                                                    AND client-rech.nom_cli MATCHES "*" + zzvchar2 + "*" NO-LOCK:
                    ASSIGN vcod_rech_cli = rechac.cod_tiers.
                    find CLIENTR where clientr.cod_cli = vcod_rech_cli no-lock no-error.
                END.
            END.
            ELSE /*...$A69159*/ IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN 
                find CLIENTR where clientr.typ_elem=zztypeCliRech AND clientr.nom_cli > "" AND CLIENTR.nom_cli MATCHES "*" + zzvchar + "*" AND CLIENTR.nom_cli MATCHES "*" + zzvchar2 + "*" no-lock no-error.
            ELSE /*$A69159...*/ IF rechac-actif-cli-motcle > 0 THEN DO:
                FOR EACH rechac WHERE rechac.typ_fich = wTypFich AND 
                                      rechac.typ_cha = "M" AND 
                                      rechac.decompo = SUBSTRING(zzcpt,1,rechac-actif-cli-motcle) AND 
                                      rechac.valeur MATCHES "*" + zzcpt + "*" ,
                                FIRST client-rech WHERE client-rech.cod_cli = rechac.cod_tiers
                                                    AND client.typ_elem = zztypeCliRech
                                                    AND client-rech.mot_cle MATCHES "*" + zzcpt + "*" NO-LOCK:
                    IF AVAIL rechac THEN DO: 
                        ASSIGN vcod_rech_cli = rechac.cod_tiers.
                        find CLIENTR where clientr.cod_cli = vcod_rech_cli no-lock no-error.
                    END.
                END.
            END.
            ELSE DO: /*...$A69159*/
                find CLIENTR where clientr.typ_elem=zztypeCliRech AND CLIENTR.mot_cle=zzcpt no-lock no-error.
            END. /*$A69159*/
            if not available clientr then 
                find CLIENTR where clientr.typ_elem=zztypeCliRech AND CLIENTR.nom_cli begins zzcpt no-lock no-error.
            if zzphoneCliRech="yes" AND not available clientr then DO:
                IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2="" THEN 
                    find clientr where clientr.typ_elem=zztypeCliRech AND clientr.phone > "" and clientr.phone MATCHES "*" + zzvchar + "*" no-lock no-error.
                ELSE IF SUBSTR(zzcpt,1,1)=" " AND zzvchar2<>"" THEN 
                    find clientr where clientr.typ_elem=zztypeCliRech AND clientr.phone > "" and clientr.phone MATCHES "*" + zzvchar + "*" AND clientr.phone MATCHES "*" + zzvchar2 + "*" no-lock no-error.
                ELSE 
                    find clientr where clientr.typ_elem=zztypeCliRech AND clientr.phone begins zzvchar no-lock no-error.
            END.
        END.
    end.
    else if substr(zzcpt,1,1)="0" or length(trim(zzcpt))>6 then do:
        IF zztypeCliRech=? THEN find CLIENTR where CLIENTR.notel begins zzcpt no-lock no-error.
        ELSE find CLIENTR where clientr.typ_elem=zztypeCliRech AND CLIENTR.notel begins zzcpt no-lock no-error.
    END.
    else find CLIENTR where CLIENTR.cod_cli = zzvnum no-lock no-error.

    if available CLIENTR THEN RUN droittyp ("RE","C",CLIENTR.typ_elem,0,OUTPUT pas-droit-type).

    /* trouv�  */
    if available CLIENTR and lookup(string(clientr.statut,"9"),zzstatut)=0 AND pas-droit-type=NO then do:
        if zzcde="C" then do:
            find typelem where typelem.typ_fich="C" and typelem.typ_elem=clientr.typ_elem no-lock no-error.
            if available typelem and typelem.commande then zzok=yes.
        end.
        else zzok=yes.
        IF pas-droit-com AND clientr.commerc[1]<>user-int THEN zzok=NO.
        ELSE IF pas-droit-tlv AND clientr.cod_tlv<>user-int THEN zzok=NO.
        ELSE IF pas-droit-res AND clientr.interv[1]<>user-int THEN zzok=NO.
        ELSE IF pas-droit-pri AND clientr.interv[2]<>user-int THEN zzok=NO.
        ELSE IF pas-droit-aux AND clientr.interv[3]<>user-int AND clientr.interv[4]<>user-int AND clientr.interv[5]<>user-int THEN zzok=NO.
        ELSE IF pas-droit-rpa AND clientr.interv[1]<>user-int AND clientr.interv[2]<>user-int AND clientr.interv[3]<>user-int AND clientr.interv[4]<>user-int AND clientr.interv[5]<>user-int THEN zzok=NO.
    end.
    else if available clientr THEN DO:
        IF pas-droit-type THEN 
            MESSAGE Traduction("Client",-2,"") + " " trim(clientr.nom_cli) 
            skip Traduction("a un statut",-2,"") + " " + ":" + " " + Traduction("interdit de recherche pour votre profil",-2,"") view-as alert-box info.
        ELSE MESSAGE Traduction("Client",-2,"") + " " trim(clientr.nom_cli) 
            skip Traduction("a un statut",-2,"") + " " + ":" + "" + " "
                  (if clientr.statut=1 then Traduction("interdit commande",-2,"")
                   else if clientr.statut=2 then Traduction("interdit livraison",-2,"")
                   else if clientr.statut=3 then Traduction("contentieux",-2,"")
                   else if clientr.statut=4 then Traduction("interdit commande et livraison",-2,"")
                   else Traduction("� supprimer",-2,"")) 
            SKIP clientr.com_sta 
            SKIP Traduction("Verrouill� le",-2,"") + " " + STRING(clientr.dat_sta) 
            SKIP Traduction("A d�bloquer par",-2,"") + " " + clientr.qui_sta view-as alert-box info.
    END.

    if zzok then assign zzinti = CLIENTR.nom_cli zzcpt = string(CLIENTR.cod_cli).
    /* non trouv� ou non valide */
    else do:
        zzpcpt=zzcpt.
        /* RECHERCHE CLIENT */
        run reccli_b (zzcde,zzstatut,input-output zzpcpt, output zzpinti).
        if zzpcpt <> "" then assign zzcpt = zzpcpt zzinti = zzpinti.
        else zzcpt="".
    end.
END PROCEDURE.
