 /* Auteur : VR */
/* 
$OPT PPE 07/05/10 on limite les acc�s � parsoc 
$A77998 CGU 27/05/14 Optimisation pour Miko
*/
/********************************************************************************
      !       ATTENTION LES ELEMENTS SUIVANTS POINTENT SUR DES TABLES TEMPORAIRES
     ! !         - enttab
    !   !        - tabgco
   !  !  !       - fctdiv
  !   !   !      - typdec
 !    !    !     - tabcomp
!!!!!!!!!!!!!    - parvs $A77998
                 - pards $A77998
              SI UTILISER A PARTIR  restor2.p
*********************************************************************************/

FUNCTION JourAnnee RETURNS INT (INPUT vtoday AS DATE) :
    DEF VAR i AS INT no-undo.
    /* jour de l'annee */   
    i = vtoday - DATE (1, 1, YEAR (vtoday)) + 1.
    RETURN i.
END FUNCTION.

FUNCTION SemaineAnnee RETURNS INT (INPUT dateCalcul AS DATE) :
    DEF VAR psemaine AS INT NO-UNDO.
    DEF VAR pannee AS INT NO-UNDO.

    RUN SemaineAnneeV2(dateCalcul,OUTPUT psemaine,OUTPUT pannee).
    RETURN psemaine.
END FUNCTION.

PROCEDURE SemaineAnneeV2:
    DEF INPUT PARAM dateCalcul AS DATE no-undo.
    DEF OUTPUT PARAM psemaine AS INT no-undo.
    DEF OUTPUT PARAM pannee AS INT no-undo.

    IF NOT AVAIL ParSoc THEN FIND FIRST parsoc NO-LOCK NO-ERROR.

    DEF VAR WdateD AS DATE no-undo. /*Dernier jour semaine*/
    DEF VAR WdateP AS DATE no-undo. /*Premier jour semaine*/
    DEF VAR Wi AS INT no-undo. 
    DEF VAR DateDebutAnneeTest AS DATE no-undo.
    DEF VAR DateDebutAnneeCalc AS DATE no-undo.
    
    ASSIGN 
        WdateD = dateCalcul
        Wi = IF parsoc.jour_deb=1 THEN 7 ELSE parsoc.jour_deb - 1.

    DO WHILE WEEKDAY(WdateD) <> Wi : /*Recherche dernier jour de la semaine*/
      WdateD = WdateD + 1.
    END.
    WdateP = WdateD - 6.

    IF YEAR(WdateP) < YEAR(dateCalcul) THEN do:
       DateDebutAnneeTest = DATE(1,1 + (parsoc.jour_deb - WEEKDAY(DATE(1,1,YEAR(dateCalcul))) + 7) MOD 7,YEAR(dateCalcul)).
       DateDebutAnneeCalc = if day(datedebutanneeTest) < 5 
                            then DATE(1,1 + (parsoc.jour_deb - WEEKDAY(DATE(1,1,YEAR(WdateP))) + 7) MOD 7,YEAR(WdateP))
                            ELSE DateDebutAnneeTest.
    END.
    ELSE IF YEAR(WdateD) > YEAR(dateCalcul) THEN do:
       DateDebutAnneeTest = DATE(1,1 + (parsoc.jour_deb - WEEKDAY(DATE(1,1,YEAR(WdateD))) + 7) MOD 7,YEAR(WdateD)).
       DateDebutAnneeCalc = if day(datedebutanneeTest) < 5 
                            THEN DATE(1,1 + (parsoc.jour_deb - WEEKDAY(DATE(1,1,YEAR(dateCalcul))) + 7) MOD 7,YEAR(dateCalcul))
                            ELSE DateDebutAnneeTest.
    END.
    ELSE ASSIGN DateDebutAnneeCalc = DATE(1,1 + (parsoc.jour_deb - WEEKDAY(DATE(1,1,YEAR(dateCalcul))) + 7) MOD 7,YEAR(dateCalcul)).
    
    pannee = YEAR(DateDebutAnneeCalc).

    IF NOT parsoc.sem_cpl AND DAY(DateDebutAnneeCalc) > 4 THEN DateDebutAnneeCalc = DateDebutAnneeCalc - 7.
    psemaine = INT(TRUNCATE((dateCalcul - DateDebutAnneeCalc) / 7,0) + 1).
END PROCEDURE.


FUNCTION DecadeAnnee RETURNS INT (INPUT vtoday AS DATE) :
    RETURN INT (TRUNC  (((JourAnnee  (vtoday) - 1) / 10) + 1 , 0)).
END FUNCTION.

FUNCTION JourAnneeDate RETURNS DATE (INPUT vjour AS INT, INPUT vannee AS INT) :
    IF vannee = ? THEN vannee = YEAR (TODAY).
    RETURN DATE (01,01,vannee) + vjour  - 1.
END FUNCTION.

FUNCTION MoisAnneeDate RETURNS DATE (INPUT vmois AS INT, INPUT vannee AS INT,INPUT vdebut AS LOG) :
    IF vannee = ? THEN vannee = YEAR (TODAY).

    IF vdebut THEN
        RETURN DATE (vmois,01,vannee).
    ELSE 
    do:
        IF vmois = 12 THEN RETURN DATE (12,31,vannee).
        ELSE RETURN DATE (vmois + 1,01,vannee) - 1.
    END.
END FUNCTION.

FUNCTION YearAnneeDate RETURNS DATE (INPUT vannee AS INT,INPUT vdebut AS LOG) :
    IF vannee = ? THEN vannee = YEAR (TODAY).
    IF vdebut THEN
        RETURN DATE (01,01,vannee).
    ELSE RETURN DATE (01,01,vannee + 1) - 1.
END FUNCTION.

FUNCTION DecadeAnneeDate RETURNS DATE (INPUT vdecade AS int, INPUT vannee AS INT, INPUT vdebut AS LOG) :
    DEF VAR tmpdate AS DATE no-undo.
    IF vannee = ? THEN vannee = YEAR (TODAY).

    IF vdebut THEN RETURN DATE (01,01,vannee) + (vdecade - 1) * 10.
    ELSE
    DO:
        tmpdate = DATE (01,01,vannee) + vdecade * 10 - 1.
        IF YEAR (tmpdate) <> vannee THEN RETURN  DATE (01,01,vannee + 1) - 1.
        ELSE RETURN tmpdate.
    END.
END FUNCTION.

FUNCTION SemaineAnneeDate RETURNS DATE (INPUT vsemaine AS int, INPUT vannee AS INT, INPUT vdebut AS LOG) :
DEF VAR DateDebutAnnee AS DATE no-undo.
IF vannee = ? THEN vannee = YEAR (TODAY).

/*$OPT...*/ IF NOT AVAIL parsoc THEN /*...$OPT*/
find first parsoc no-lock no-error.
DateDebutannee = date(1,1 + (parsoc.jour_deb - weekday(date(1,1,vannee)) + 7) mod 7,vannee).
/* 1er jour de la premiere semaine */
if parsoc.sem_cpl=no and day(datedebutannee)>4 then datedebutannee = datedebutannee - 7.

IF vdebut THEN RETURN datedebutannee + (7 * vsemaine) - 7.
ELSE RETURN datedebutannee + (7 * vsemaine) - 1.

END FUNCTION.

