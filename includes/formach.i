/* Auteur : HL
Fin du format du prix en fonction des d�cimales g�r�es � l'achat 
N�cessite l'include {decia.i}
*/
/********************************************************************************
      !       ATTENTION LES ELEMENTS SUIVANTS POINTENT SUR DES TABLES TEMPORAIRES
     ! !         - focondi
    !   !        - clcondi
   !  !  !
  !   !   !
 !    !    !
!!!!!!!!!!!!  SI UTILISER A PARTIR EDTCOUV2.P
*********************************************************************************/
FUNCTION FormAch RETURNS CHAR (INPUT devise AS CHAR):
    if Devise=g-dftdev or Devise="" then RETURN fmta.
    else do:
        find first tabcomp where type_tab="DE" and a_tab=Devise no-lock no-error.     
        IF AVAIL tabcomp THEN RETURN (IF deb_pays = 0 THEN "9" ELSE "9." + FILL("9",deb_pays)).
        ELSE RETURN fmta.
    end.
END FUNCTION.
