/*bois-api.i
APR 16/11/06 fonctions utiles pour le bois
$A63854  DCA 25/06/13 Ajout fonctions transfert ligne et colis bois (Evolutions dans le standard cr��es pour Rougier)
$A63854  TBE 22/07/13 Ajout fonction dispo de toutes les grumes pour un colis (Evolutions dans le standard cr��es pour Rougier)
$A63854 DCA 15/11/13 Ajout fonction de mise � jour des colis pr�visionnel
$A77998 CGU 27/05/14 Optimisation pour Miko
*/
/********************************************************************************
      !       ATTENTION LES ELEMENTS SUIVANTS POINTENT SUR DES TABLES TEMPORAIRES
     ! !         - enttab
    !   !        - tabgco
   !  !  !       - fctdiv
  !   !   !      - typdec
 !    !    !     - tabcomp
!!!!!!!!!!!!!    - parvs $A77998
                 - pards $A77998
              SI UTILISER A PARTIR  restor2.p
*********************************************************************************/

&IF DEFINED(G-hBois) = 0 &THEN
&GLOBAL-DEFINE G-hBois G-hBois
DEF VAR G-hBois AS HANDLE NO-UNDO.
DEF VAR G-hBois### AS C NO-UNDO.
G-hBois### = GetSessionData ("BOIS":U,"CLE":U).
IF G-hbois### <> "NO" THEN G-hBois = WIDGET-HANDLE(G-hBois###) NO-ERROR.

/*FUNCTION IsProduitBois RETURNS LOGICAL (INPUT iTypElem AS CHARACTER) IN G-Hbois.*/
/*V�rifie que le type de produit est un produit bois*/
FUNCTION IsProduitBois RETURNS LOGICAL (INPUT iTyp_Elem AS CHARACTER):
    RETURN (G-hbois### <> "NO") AND CAN-FIND(FIRST typelem WHERE typelem.typ_fich = "P" AND typelem.typ_elem = ityp_elem AND typelem.typ_bois = YES NO-LOCK).
END FUNCTION.
/*********************************************************************************************************************************************************/

FUNCTION NumeroAutoColis-bois RETURNS CHARACTER IN G-hbois.

FUNCTION Verif-param-utilise RETURNS LOGICAL (INPUT iStyp_par AS CHARACTER) IN G-hbois.
FUNCTION Verif-produit-utilise RETURNS LOGICAL (INPUT iTyp_elem AS CHARACTER,INPUT icod_pro AS INT) IN G-hbois.

FUNCTION GET-constante-bois RETURNS CHARACTER (INPUT iCode AS CHAR,INPUT iDef AS CHAR) IN G-hbois.

FUNCTION TypVar-bois RETURNS CHARACTER (INPUT iType AS CHAR) IN G-Hbois.

FUNCTION IsVarMesure RETURNS LOGICAL (INPUT iK_var AS CHARACTER) IN G-Hbois.
FUNCTION IsVarColis RETURNS LOGICAL (INPUT iCod_pro AS INT,INPUT iTypVar AS CHARACTER) IN G-HBois.
FUNCTION Obtenir-kvar-bois RETURNS CHAR (INPUT iCod_pro AS INT,INPUT iK_var AS CHAR) IN G-Hbois.

FUNCTION Suppr-produit-bois RETURNS LOGICAL (INPUT iTyp_elem AS CHARACTER,INPUT iCod_pro AS INTEGER) IN G-hbois.

FUNCTION INFO-mesure-bois RETURNS LOGICAL (INPUT iCod_pro AS INTEGER,INPUT iTyp_mes AS CHAR,INPUT iCod_var AS CHARACTER,OUTPUT aFormat AS CHAR,OUTPUT aUnite AS CHAR) IN G-hbois.
FUNCTION Coef-unite-bois RETURNS DECIMAL (INPUT iUnite AS CHAR) IN G-hbois.
FUNCTION Executer-fctinit-bois RETURNS LOGICAL (INPUT iCod_pro AS INT,INPUT iDepot AS INT,INPUT iParSup AS CHAR,INPUT-OUTPUT iKvar_mes AS CHAR, INPUT-OUTPUT iKVar_col AS CHAR) IN G-hbois.

FUNCTION Gestion-dev-bois RETURNS LOGICAL (INPUT iCod_pro AS INTEGER) IN G-hbois.
FUNCTION Gestion-detail-bois RETURNS LOGICAL (INPUT iCod_pro AS INTEGER) IN G-hbois.

FUNCTION Info-produit-bois RETURNS CHARACTER (INPUT iCod_pro AS INTEGER,INPUT iTyp AS CHARACTER) IN G-hbois.

FUNCTION PR-lot-produit-bois RETURNS LOGICAL (INPUT iCod_pro AS INTEGER) IN G-hbois.

FUNCTION convertit-mesure RETURNS DECIMAL (INPUT iMes AS DECIMAL,INPUT iUnit1 AS CHAR,INPUT iUnit2 AS CHAR) IN G-hbois.

FUNCTION Recherche-Conversion-Mesure RETURNS LOGICAL (INPUT iTyp_elem AS CHAR, INPUT iCod_pro AS INT, INPUT iTyp_var AS CHAR,
                                            INPUT iDEC_achat AS DEC,INPUT iUnit_achat AS CHAR,INPUT iUnit_conv AS CHAR,INPUT iUnit_mes AS CHAR,
                                            OUTPUT oDec_conv AS DEC,OUTPUT oDec_mes AS DEC) IN G-hbois.

FUNCTION CODE-produit-detail RETURNS INTEGER (INPUT iCod_pro AS INTEGER,INPUT iK_var AS CHARACTER) IN G-hbois.

function ObtenirVarianteBois returns logical (input iRid_lig as rowid,INPUT iTypVar AS char,OUTPUT oValVar AS CHAR) IN G-hbois.

FUNCTION NumLot-bois RETURNS CHARACTER (INPUT iTyp_mvt AS CHARACTER,INPUT iCod_cf AS INTEGER,INPUT iNo_cde AS INTEGER,INPUT iNo_BL_A AS CHARACTER,INPUT iNum_colis AS CHARACTER,INPUT iCod_pro AS INTEGER,INPUT iEss AS CHARACTER,INPUT iSaisie AS CHARACTER,INPUT iDatRec AS DATE,INPUT iCompteur AS INT) IN G-hbois.
FUNCTION NumLot-boisV2 RETURNS CHARACTER (INPUT iTyp_mvt AS CHARACTER,INPUT iCod_cf AS INTEGER,INPUT iNo_cde AS INTEGER,INPUT iNo_BL_A AS CHARACTER,INPUT iNum_colis AS CHARACTER,INPUT iCod_pro AS INTEGER,INPUT iK_var AS CHARACTER,INPUT iSaisie AS CHARACTER,INPUT iDatRec AS DATE,INPUT iCompteur AS INT) IN G-hbois.

FUNCTION NumLot-saisi RETURNS LOGICAL IN G-hbois.

/*pas utilis�e pour le moment*/
FUNCTION Modif_num_colis RETURNS LOGICAL (INPUT iRid_boiscol AS ROWID,INPUT iNum_colis AS CHARACTER) IN G-hbois.

/*FUNCTION Modif_no_bl_bois RETURNS LOGICAL (INPUT iRid_ent AS ROWID,INPUT iNo_BL AS CHARACTER,INPUT iDat_rec AS DATE) IN G-hbois.*/

FUNCTION Sup-Colis-Bois RETURNS LOGICAL (INPUT iCod_cf AS INTEGER,INPUT iNo_cde AS INTEGER,INPUT iNum_colis AS CHARACTER) IN G-hbois.

FUNCTION Sup-LigneFou-Bois RETURNS LOGICAL (INPUT iTyp_mvt AS CHARACTER,INPUT iCod_cf AS INTEGER,INPUT iNo_cde AS INTEGER,INPUT iNum_colis AS CHARACTER,INPUT iNo_ligne AS INTEGER) IN G-hbois.

FUNCTION changer-cdefou-colis-bois RETURNS LOGICAL (INPUT rid_newlig AS ROWID,INPUT rid_oldlig AS ROWID) IN G-hbois.

FUNCTION Sup-LigneCli-Bois RETURNS LOGICAL (INPUT iROWID AS ROWID) IN G-hbois.

FUNCTION Creation-reception-colis-bois RETURNS LOGICAL (INPUT iTyp_mvt AS CHARACTER,INPUT iROWID AS ROWID,INPUT iNo_BL_A AS CHARACTER,INPUT iDatRec AS DATE,INPUT iCompteur AS INT,INPUT iSaisie AS CHARACTER,INPUT iMagasin AS INTEGER,INPUT iEmplac AS CHARACTER) IN G-hbois.

FUNCTION Creation-reception-ligne-bois RETURNS LOGICAL (INPUT iTyp_mvt AS CHARACTER,INPUT iRowid AS ROWID,INPUT iNum_colis AS CHARACTER,INPUT iNum_colis AS CHARACTER,INPUT iNo_ligne AS INTEGER) IN G-hbois.

/*Ne sont plus appel�es directement, passer par transfert-depot-... 
FUNCTION Maj-depot-colis-bois RETURNS ROWID(INPUT iRowid AS ROWID,INPUT iDepot AS INTEGER,INPUT iNo_bl AS INTEGER,INPUT iDat_mvt AS DATE,INPUT iRef_mvt AS CHAR) IN G-hbois.
FUNCTION Maj-depot-ligne-bois RETURNS ROWID(INPUT iRowid AS ROWID,INPUT IRid_lig AS ROWID,INPUT iDepot AS INTEGER,INPUT iNo_bl AS INTEGER,INPUT iDat_mvt AS DATE,INPUT iRef_mvt AS CHAR,INPUT iQte AS DEC,INPUT iDev AS DEC) IN G-hbois.
*/
FUNCTION Maj-emplacement-colis-bois RETURNS LOGICAL(INPUT iRowid AS ROWID,INPUT iMagasin AS INTEGER,INPUT iEmplac AS CHARACTER) IN G-hbois.

/*FUNCTION format-mesure-bois RETURNS CHARACTER (INPUT iTyp_elem AS CHAR,INPUT icod_pro AS INT,INPUT iTyp_mes AS CHAR,INPUT iCod_var AS CHAR) IN G-hbois.*/

FUNCTION Calcul-vol-poids-vente-bois RETURNS LOGICAL (input iRowid as ROWID,OUTPUT ovol AS DECIMAL,OUTPUT oPds AS DECIMAL) IN G-hbois.
FUNCTION Calcul-vol-poids-venteH-bois RETURNS LOGICAL (input iRowid as ROWID,OUTPUT ovol AS DECIMAL,OUTPUT oPds AS DECIMAL) IN G-hbois.
FUNCTION Calcul-vol-poids-achat-bois RETURNS LOGICAL (input iRowid as ROWID,OUTPUT ovol AS DECIMAL,OUTPUT oPds AS DECIMAL) IN G-hbois.

/*A virer apr�s passage en 4.2 (n�cessaire pour le filtre)*/
FUNCTION Calcul-vol-achete-bois RETURNS DECIMAL (input iRowid as ROWID) IN G-hbois. 
function Calcul-volume-achat-bois returns logical (input iRowid as rowid,output oVol_ach as decimal,output ovol_cnv as decimal,output ovol_mes as decimal) IN G-hbois. 

FUNCTION Calcul-vol-achat-ct-bois RETURNS DECIMAL (INPUT iEncours AS LOG,input iRowid as ROWID) IN G-hbois.


function Calcul-volume-bois returns logical (input iRowid as rowid,output oVol_ach as decimal,output ovol_cnv as decimal,output ovol_mes as decimal) in G-hbois.

function Calcul-vol-ach-bois returns logical (input iRowid as rowid,output oVol_ach as decimal) IN G-hbois.

function Calcul-vol-stock-bois returns decimal (input iRowid as rowid,output oVol_ach as decimal,output ovol_cnv as decimal,output ovol_mes as decimal) IN G-hbois.

function Creation-sortie-bois returns logical (input iRid_entetcli as rowid,INPUT iType AS char,INPUT iRid_ligne AS ROWID,INPUT iQte AS DECIMAL,INPUT iDev AS DECIMAL,INPUT iNo_ctbois AS INT,INPUT iNo_ligct AS INT,INPUT iParsup AS char,INPUT-OUTPUT aLien_nmc AS INTEGER,OUTPUT oRid_ligne AS ROWID) IN G-hbois.

function Copie-sortie-bois returns logical (input iRid_ori as rowid,INPUT iRid_dest AS ROWID,INPUT iParsup AS CHAR,INPUT iDev AS DECIMAL) IN G-hbois.

function Copie-sortie-bois-lignecli returns logical (input iRid_ori as rowid,INPUT iRid_dest AS ROWID,INPUT iParsup AS CHAR,INPUT iDev AS DECIMAL) IN G-hbois.

FUNCTION Maj-Date-sortie-bois RETURNS LOG (INPUT iType AS CHAR,INPUT iRid_ent AS ROWID) IN G-hbois.

FUNCTION Maj-qte-lignecli-bois RETURNS LOGICAL (input iNo_mvtbois AS INTEGER,INPUT iAvoir AS LOGICAL,INPUT iDiff_qte AS DECIMAL,INPUT iDev AS DECIMAL) IN G-hbois.

FUNCTION calcul-vol-fct-bois returns decimal (INPUT iCod_pro_g AS INTEGER, INPUT iQte AS DECIMAL, INPUT iTyp_mes1 AS CHARACTER,INPUT iTyp_mes2 AS CHARACTER,
                                                   INPUT iK_var AS CHARACTER,INPUT iFct_vol AS CHARACTER) IN G-hbois.

function Calcul-volume-vente-bois returns logical (INPUT iCod_pro_g AS INTEGER, INPUT iQte AS DECIMAL, INPUT iTyp_mes AS CHARACTER,
                                                   INPUT iK_var AS CHARACTER,INPUT iUni_fac AS CHARACTER,INPUT iCod_conv AS INTEGER, OUTPUT ovolume AS DECIMAL) IN G-hbois.

FUNCTION calcul-vol-sorti-bois RETURNS DECIMAL (INPUT iCod_pro AS INTEGER, INPUT iQte AS DECIMAL, INPUT iK_var AS CHARACTER) IN G-hbois.

FUNCTION Calcul-volume-vente-M3-Bois returns DECIMAL (input iRowid as rowid) IN G-hbois.

FUNCTION ReCalcul-volume-colis-vente-bois RETURNS LOGICAL (INPUT iRid_lignecli_col AS ROWID) IN G-hbois.

FUNCTION Reprise-devis-Bois RETURNS LOGICAL (INPUT iROWID1 AS ROWID,INPUT iROWID2 AS ROWID,INPUT iDev AS DECIMAL) IN G-hbois.

FUNCTION copie-ligne-devis-bois RETURNS LOGICAL (INPUT iRid_ent AS ROWID,INPUT iRowid AS ROWID,INPUT-OUTPUT derlig AS INT, INPUT iParam AS CHARACTER) IN G-HBois.

FUNCTION devis-commande-bois RETURNS LOGICAL (INPUT iRowid AS ROWID,INPUT iParam AS CHARACTER) IN G-hbois.

FUNCTION Livraison-LigneCli-Bois RETURNS LOGICAL (INPUT irid_ent AS ROWID,INPUT iROWID AS ROWID,INPUT iNb_decal AS INT) IN G-hbois.

FUNCTION Renumeroter-lignecli RETURNS LOGICAL (INPUT iROWID AS ROWID,INPUT lig_dep AS INT,INPUT nb_Decal AS INT) IN G-hbois.

FUNCTION Annuler-Livraison-LigneCli-Bois RETURNS LOGICAL (INPUT iROWID AS ROWID) IN G-hbois. 

FUNCTION Livraison-Std-Bois RETURNS LOGICAL (INPUT iROWID AS ROWID,INPUT lst_sel AS CHAR) IN G-hbois.

FUNCTION Annuler-TransfertDepot-Bois RETURNS LOGICAL (INPUT iRowid AS ROWID) IN G-hbois.

FUNCTION Annuler-SortieAutre-Bois RETURNS LOGICAL (INPUT iRowid AS ROWID) IN G-hbois.

FUNCTION Maj-Stat-Bois RETURNS LOGICAL (INPUT iROWID AS ROWID) IN G-hbois.

FUNCTION Calcul_qte_dispo RETURNS DECIMAL (INPUT iNo_mvtbois AS INTEGER,OUTPUT aDev AS DEC) IN G-hbois.

FUNCTION Calcul-qte-STOCK RETURNS DECIMAL (INPUT iRowid AS ROWID,INPUT iDat_stk AS DATE,OUTPUT oDev AS DECIMAL) IN G-hbois.

FUNCTION Calcul_qte_max_avoir RETURNS DECIMAL (INPUT iNo_mvtbois AS INTEGER,OUTPUT aDev AS DECIMAL) IN G-hbois.

FUNCTION Modif-variante-lignecli RETURN LOGICAL (INPUT iRowid AS ROWID,INPUT aTyp_var AS CHARACTER,INPUT iParam AS CHARACTER) IN G-hbois.

FUNCTION Modif-unite-lignecli RETURN LOGICAL (INPUT iRowid AS ROWID,INPUT iUnite AS CHARACTER,INPUT iRecalPx AS LOGICAL) IN G-hbois.

FUNCTION Creation-transfo-bois returns logical (input iRid_entetfou as rowid,INPUT iRid_ligne AS ROWID,INPUT iQte AS DECIMAL,INPUT iDev AS DECIMAL,INPUT iNo_mvtbois AS INTEGER,INPUT iParsup AS CHAR) IN G-hbois.

/*function Calcul-vol-sciage-bois returns logical (input iRowid as rowid,output oVol_mes as decimal) IN G-hbois.*/

FUNCTION Maj-tt_recalcol RETURNS LOGICAL (INPUT iencours AS LOGICAL,INPUT iTyp_mvt AS CHAR,INPUT iCod_cf AS INT,INPUT iNo_cde AS INT,INPUT iNo_bl AS INT,INPUT iLien_nmc AS INT) IN G-hbois.

function Suppression-saisie-sciage-bois RETURNS LOGICAL (input iRowid as rowid) IN G-hbois.

function Suppression-cde-sciage-bois RETURNS LOGICAL (input iRowid as rowid,INPUT ino_mvtbois AS INTEGER,INPUT Sup_saisie AS LOGICAL) IN G-hbois.

FUNCTION solder-cde-sciage-sechage-bois RETURNS LOGICAL (INPUT iRid_entetfou AS ROWID,INPUT iDt_sortie AS DATE,OUTPUT oRid_histoent AS ROWID) IN G-hbois.

FUNCTION Calcul-prix-sciage RETURNS LOGICAL (INPUT iCod_cf AS INT,INPUT iNo_cde AS INT,INPUT iNo_mvtbois AS INTEGER) IN G-hbois.

FUNCTION Maj-px-sorties-bois RETURNS LOGICAL (INPUT iRid_lig AS ROWID) IN G-hbois.

FUNCTION Renum-lignecli-bois RETURNS LOGICAL (INPUT iRowid AS ROWID,INPUT iNo_ligne AS INTEGER) IN G-hbois.

FUNCTION Verif-devis-commande-bois RETURNS LOGICAL (INPUT iRowid AS ROWID,OUTPUT Qte_dispo AS DECIMAL,OUTPUT Dev_dispo AS DECIMAL) IN G-hbois.

FUNCTION Verif-suppr-entetcli-bois RETURNS LOGICAL (INPUT iRowid AS ROWID,OUTPUT numerr AS INTEGER) IN G-hbois.
FUNCTION Verif-suppr-lignecli-bois RETURNS LOGICAL (INPUT iRowid AS ROWID,OUTPUT numerr AS INTEGER) IN G-hbois.

FUNCTION MAJ-recalstk-bois RETURNS LOGICAL (INPUT hbuf AS HANDLE) IN G-hbois.

FUNCTION recalstk-bois RETURNS LOGICAL IN G-hbois.

FUNCTION Calcul-prix-cascade-bois RETURNS LOGICAL (INPUT iRecal_premier AS LOGICAL,INPUT iDOS_pxr AS INTEGER) IN G-hbois.

FUNCTION selectVariantes RETURNS LOGICAL(INPUT Kvar AS CHARACTER,INPUT Selectvar AS CHARACTER) IN G-hbois.

FUNCTION prix-net-colis RETURNS LOGICAL(INPUT iRowid AS ROWID,OUTPUT PxAch AS DEC,OUTPUT PxRvt AS DEC) IN G-hbois.

FUNCTION recalcul-pr-dospxr RETURNS LOGICAL (INPUT iDosPxr AS INT) IN G-hbois.

FUNCTION cherche-ligne-origine RETURNS ROWID (INPUT iRid_lignecli AS ROWID) IN G-hbois.

FUNCTION Verif-etat-ligne-bois RETURNS INTEGER (INPUT iRid_histolig AS ROWID,INPUT iDate AS DATE) IN G-hbois.

FUNCTION Verif-etat-commande-bois RETURNS INTEGER (INPUT iTyp_sai AS CHAR, INPUT iCod_cli AS INT,INPUT iNo_cde AS INT,INPUT iDate AS DATE) IN G-hbois.

FUNCTION Verif-sechage-ligne-bois RETURNS LOGICAL (INPUT iRid_histolig AS ROWID) IN G-Hbois.
FUNCTION Verif-sechage-colis-bois RETURNS LOGICAL (INPUT iRid_boiscol AS ROWID) IN G-Hbois.
FUNCTION Verif-sechage-commande-bois RETURNS LOGICAL (INPUT iTyp_sai AS CHAR, INPUT iCod_cli AS INT,INPUT iNo_cde AS INT) IN G-Hbois.

FUNCTION Commande-vente-contrat-bois RETURNS LOGICAL (INPUT iRid_entete AS ROWID,INPUT iCreat AS LOGICAL,OUTPUT aRid_entetcli AS ROWID) IN G-hbois.

FUNCTION maj-qte-contrat-bois RETURNS LOGICAL (INPUT iNO_ctbois AS INT,INPUT iNO_ligct AS INT,INPUT iRid_lignecli AS ROWID,INPUT iParsup AS CHAR) IN G-hbois.

FUNCTION Verif-commande-contrat-bois RETURNS INTEGER (INPUT iRid_entetcli AS ROWID) IN G-hbois.

FUNCTION Transfert-commande-vente-bois RETURNS LOGICAL (INPUT iRid_lignecli AS ROWID,INPUT iRid_entetcli AS ROWID,INPUT iNo_ctbois AS INT,INPUT iNo_ligct AS INT) IN G-hbois.

FUNCTION recalcul-commande-vente-bois RETURNS LOGICAL (INPUT iRid_entetcli AS ROWID) IN G-hbois.

FUNCTION maj-prix-ligne-commande-bois RETURNS LOGICAL (INPUT iRid_lignecli AS ROWID,INPUT iPx_ven AS DECIMAL) IN G-hbois.

FUNCTION Modif-mesure-ligne-commande-bois RETURNS LOGICAL (INPUT iRid_lignecli AS ROWID,INPUT ptyp AS CHARACTER) IN G-hbois.

FUNCTION recalcul-commission-contrat RETURNS LOGICAL (INPUT iRid_entetcli AS ROWID,INPUT iNo_ctbois AS INT,INPUT iNo_ligct AS INT) IN G-hbois.

FUNCTION sup-ligfou-bois RETURNS LOGICAL (INPUT rid_lignefou AS ROWID) IN g-hbois.

FUNCTION synchro-cdefou-bois RETURNS LOGICAL (INPUT iRid_lignecli AS ROWID,INPUT iRid_lignefou AS ROWID,INPUT iLigfou_ori AS INT) IN G-hbois.

FUNCTION MAJ-variantes-bois RETURN LOG (INPUT pNo_mvtbois AS INT,INPUT pk_var AS CHAR,INPUT pmd5 AS CHAR) IN G-hbois.

IF G-hBois### <> "NO" AND NOT VALID-HANDLE (G-hBois) THEN DO:
    IF NOT AVAILABLE(parsoc) THEN FIND FIRST parsoc NO-LOCK NO-ERROR.
    IF AVAILABLE parsoc AND parsoc.bois_ar THEN DO:
        RUN bois-api PERSISTENT SET G-hBois.
        SetSessionData ("BOIS":U,"CLE":U,STRING(G-hBois)).
    END.
    ELSE SetSessionData ("BOIS":U,"CLE":U,"NO").
END.


/*$A63854...*/
FUNCTION transfert-colis-bois RETURNS LOGICAL(INPUT iRowid AS ROWID,INPUT iDepot AS INTEGER,INPUT iNo_bl AS INTEGER,INPUT iDat_mvt AS DATE,INPUT iRef_mvt AS CHAR,INPUT iMagasin AS INTEGER,INPUT iEmplac AS CHARACTER) IN G-hbois.
FUNCTION transfert-ligne-bois RETURNS ROWID(INPUT iRowid AS ROWID,INPUT IRid_lig AS ROWID,INPUT iDepot AS INTEGER,INPUT iNo_bl AS INTEGER,INPUT iDat_mvt AS DATE,INPUT iRef_mvt AS CHAR,INPUT iQte AS DEC,INPUT iDev AS DEC,INPUT iMagasin AS INTEGER,INPUT iEmplac AS CHARACTER) IN G-hbois.
FUNCTION transfert-colis-prev-bois RETURNS LOGICAL(INPUT iRowid AS ROWID,INPUT iDepot AS INTEGER,INPUT iNo_bl AS INTEGER,INPUT iDat_sor AS DATE,INPUT iDat_ent AS DATE,INPUT iRef_mvt AS CHAR,INPUT iMagasin AS INTEGER,INPUT iEmplac AS CHARACTER) IN G-hbois.
FUNCTION transfert-ligne-prev-bois RETURNS ROWID(INPUT iRowid AS ROWID,INPUT IRid_lig AS ROWID,INPUT iDepot AS INTEGER,INPUT iNo_bl AS INTEGER,INPUT iDat_sor AS DATE,INPUT iDat_ent AS DATE,INPUT iRef_mvt AS CHAR,INPUT iQte AS DEC,INPUT iDev AS DEC,INPUT iMagasin AS INTEGER,INPUT iEmplac AS CHARACTER) IN G-hbois.

FUNCTION annuler-transfert-ligne-prev-bois RETURNS LOGICAL (INPUT iRowid AS ROWID) IN G-hbois.
FUNCTION valider-transfert-ligne-prev-bois RETURNS LOGICAL (INPUT iRowid AS ROWID) IN G-hbois.

FUNCTION existe-sortie-bois RETURNS LOGICAL(INPUT iRowid AS ROWID,INPUT-OUTPUT pParsup AS CHAR,OUTPUT pTxt AS CHAR) IN G-hbois.

FUNCTION supprimer-colis-bois RETURNS LOGICAL(INPUT iRowid AS ROWID, INPUT-OUTPUT plibre AS CHAR) IN G-hbois.

FUNCTION maj-colis-previsionnel RETURNS LOGICAL(INPUT iRidboiscol AS ROWID, INPUT-OUTPUT plibre AS CHAR) IN G-hbois.
/*...$A63854*/
&ENDIF
