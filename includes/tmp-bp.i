/* Auteur : HL 
   Commandes s�lectionn�es pour �dition B.P 

$A66559 TCH 18/09/13 Vocal   
*/
DEF NEW SHARED TEMP-TABLE WENT NO-UNDO
  FIELD wcli AS INT  /* Client */
  FIELD wcde AS INT  /* Commande */
  FIELD wtou AS CHAR /* Tourn�e */
  FIELD word AS INT  /* Ordre dans la tourn�e */
  FIELD wadr AS CHAR /* Ancien n� B.P + D�p�t + El�ments Adresse livraison --> commandes regroup�es 
                        sinon c'est un compteur pour que chaque commande soit s�par�e */
  FIELD wregr AS INT
  FIELD wmag  AS INT /*$A66559*/
INDEX IWBP1 IS PRIMARY wtou word DESC wcli ASC wadr ASC wcde ASC
INDEX IWBP2 wcli wcde.
