/* Auteur : HL

Variables RUN CALPX
$1287 VB 23/04/07 Gestion de la 4i�me remise
$1396 HL 07/11/07 Liste de clients tarifs et cumul conditions
$1798 AP 31/03/09 Gestion des remises en valeur
A37551 HL 12/07/11 Format des remises en valeur � g�rer comme le format des prix
*/
/********************************************************************************
      !       ATTENTION LES ELEMENTS SUIVANTS POINTENT SUR DES TABLES TEMPORAIRES
     ! !         - focondi
    !   !        - clcondi
   !  !  !
  !   !   !
 !    !    !
!!!!!!!!!!!!  SI UTILISER A PARTIR EDTCOUV2.P
*********************************************************************************/

def var wno_tarif as i no-undo.
def var wpx_refv  as decimal decimals 4 no-undo.
def var wpx_vte   as decimal decimals 4 no-undo.
def var wdev_vte  as char no-undo.
def var wpx_refa  as decimal decimals 4 no-undo.
def var wpx_ach   as decimal decimals 4 no-undo.
def var wpx_ach_d as decimal decimals 4 no-undo.
def var wrem1     as decimal decimals 4 no-undo.
def var wrem2     as decimal decimals 4 no-undo.
def var wrem3     as decimal decimals 4 no-undo.
def var wrem4     as decimal decimals 4 no-undo.
def var wpromo    as char no-undo.
def var wremapp   as log no-undo.
DEF VAR wls_tarif AS CHAR no-undo.
DEF VAR wls_cumc  AS CHAR no-undo.
/*$1798...*/
Def Var wremval1 As Log No-Undo.
Def Var wremval2 As Log No-Undo.
Def Var wremval3 As Log No-Undo.
Def Var wremval4 As Log No-Undo.
/*...$1798*/
