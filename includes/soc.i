/* Auteur : JL
   Date   : 26/08/05
   Cr�ation de la combo csoc

   {1} : "T" = <Toutes soci�t�>  + Soci�t�s + D�p�ts
       : "S" = <Toutes soci�t�>  + Soci�t�s
       : "D" = Soci�t�s
       : "E" = <Toutes soci�t�>  + La Soci�t� en cours
   {2} : Soci�t� ou d�p�t si param�tre en entr�e sinon ''

   !!! Attention !!!
   Le code = "D" + N�D�p�t
             "S" + N�Soci�t�

$1070 HL 18/08/05 Multi-Soci�t� (Rechercher : parvs, n_d, parsoc.mult_soc)
A34993 HL 13/07/10 {2} = "T" : ListeDepots : contient la liste des d�p�ts autoris�s, ce qui permet de g�rer quand m�me la ligne <Tous d�p�ts> ou <Toutes soci�t�s>
                   chose qu'on ne pouvait pas faire auparavant quand l'un des d�p�ts ou l'une des soci�t�s n'�tait pas autoris�
                   {2} = "S" : ListeSocietes : contient la liste des soci�t�s autoris�es, ce qui permet de g�rer quand m�me la ligne <Toutes soci�t�s> 
                   chose qu'on ne pouvait pas faire auparavant quand l'une des soci�t�s n'�tait pas autoris�e
$SOCDEP ASO 19/04/13 Modification des d�clarations variables pour utiliser soc.i et depot.i dans un m�me programme
$A65846 HL 01/10/13 Liste des d�p�ts en ordre alpha (liste_alpha)
*/

DEF VAR ProLink    AS C NO-UNDO.
ProLink = GetSessionData("Config!", "ProLink").

DEF VAR pDosMaster AS INT  NO-UNDO.
DEF VAR Liste      AS CHAR NO-UNDO.
DEF VAR IDos       AS I NO-UNDO.
DEF VAR CurDos     AS INT  NO-UNDO.
DEF VAR CurNom     AS CHAR NO-UNDO.

DEF VAR kIntiSoc AS CHAR NO-UNDO. /* $SOCDEP */
DEF VAR kNivSoc  AS INT  NO-UNDO. /* $SOCDEP */

ASSIGN 
    ListeDepots=""
    ListeSocietes=""
    LstSoc="".

find droigco where droigco.util = g-user and droigco.fonc = "DEPOT" no-lock no-error.
IF NOT AVAIL droigco THEN find droigco where droigco.util = g-user-droit and droigco.fonc = "DEPOT" no-lock no-error.

IF parsoc.mult_soc THEN DO:
    Csoc:DELETE(1).
    RUN VALUE(ProLink + "\MultEPass.r") (0, ?, OUTPUT pDosMaster, OUTPUT Liste).
    
    X=0.
    IF Liste <> "" THEN DO IDos = 1 TO NUM-ENTRIES(Liste):
        X=X + 1.
        CurDos = INT(ENTRY(IDos, Liste)).
        IF GetUser(G-USER, "PROGIWIN", CurDos, OUTPUT kIntiSoc /* $SOCDEP */ , OUTPUT kNivSoc /* $SOCDEP */) = ? THEN NEXT.
        IF "{1}" = "E" AND CurDos <> G-ndos THEN NEXT.

        CurNom = GetDynIntitule("Dossier", "bApplic = 'POCWIN' AND NumDos = " + STRING(CurDos), "Nom").
        csoc:ADD-LAST("<" + TRIM(CurNom) + " (" + STRING(Curdos) + ")" + ">", "S" + STRING(Curdos)).
        IF "{1}" = "S" THEN 
            ListeSocietes=ListeSocietes + STRING(Curdos) + ",".

        IF "{1}" = "T" THEN DO:
            IF AVAIL droigco AND droigco.liste_alpha THEN FOR EACH Tabgco WHERE tabgco.ndos = CurDos AND tabgco.TYPE_tab = "DS" NO-LOCK BY tabgco.inti_tab:
                IF tabgco.productif=NO THEN NEXT. /* Avant x=x + 1 pour avoir quand m�me <tous d�p�ts> */
                X=X + 1.
                IF CAN-FIND(FIRST droitdep WHERE droitdep.util=g-user-droit AND droitdep.depot=tabgco.n_tab NO-LOCK) THEN
                    LstSoc=LstSoc + STRING(tabgco.ndos) + ",".
                ELSE DO:
                    csoc:ADD-LAST("    " + tabgco.inti_tab + "(" + string(tabgco.n_tab) + ")", "D" + STRING(tabgco.n_tab)).
                    ListeDepots=ListeDepots + STRING(tabgco.n_tab) + ",".
                END.
            END.
            ELSE FOR EACH Tabgco WHERE tabgco.ndos = CurDos AND tabgco.TYPE_tab = "DS" NO-LOCK BY tabgco.n_tab:
                IF tabgco.productif=NO THEN NEXT. /* Avant x=x + 1 pour avoir quand m�me <tous d�p�ts> */
                X=X + 1.
                IF CAN-FIND(FIRST droitdep WHERE droitdep.util=g-user-droit AND droitdep.depot=tabgco.n_tab NO-LOCK) THEN
                    LstSoc=LstSoc + STRING(tabgco.ndos) + ",".
                ELSE DO:
                    csoc:ADD-LAST("    " + tabgco.inti_tab + "(" + string(tabgco.n_tab) + ")", "D" + STRING(tabgco.n_tab)).
                    ListeDepots=ListeDepots + STRING(tabgco.n_tab) + ",".
                END.
            END.
        END.
    END.

    LstSoc=TRIM(LstSoc,",").

    IF "{1}" = "E" THEN 
        csoc:ADD-FIRST("" + "-" + Traduction("Toutes soci�t�s autoris�es",-2,"") + "-" + "", "0").
    ELSE IF ("{1}" = "T" OR "{1}" = "S") AND csoc:NUM-ITEMS>1 THEN DO:
        IF "{1}" = "S" THEN DO:
            IF csoc:NUM-ITEMS<>X THEN ListeSocietes=TRIM(ListeSocietes,",").
            ELSE ListeSocietes="".
        END.
        ELSE DO:
            IF csoc:NUM-ITEMS<>X THEN ListeDepots=TRIM(ListeDepots,",").
            ELSE ListeDepots="".
        END.
        csoc:ADD-FIRST("" + "-" + Traduction("Toutes soci�t�s autoris�es",-2,"") + "-" + "", "0").
    END.
    
    IF {2} <> "" AND LOOKUP({2},csoc:LIST-ITEM-PAIRS)>0 THEN 
        csoc:SCREEN-VAL = {2}.
    ELSE IF LOOKUP(vdepot,csoc:LIST-ITEM-PAIRS)>0 THEN 
        csoc:SCREEN-VAL=vdepot.
    ELSE csoc:SCREEN-VAL = csoc:ENTRY(1).
END.

/* Pour info, on ne passe jamais ici si {2} <> "T" */
ELSE DO:

    FIND tabgco where tabgco.type_tab="DS" NO-LOCK NO-ERROR.
    ASSIGN 
        logbid=csoc:DELETE(1)
        un-depot=NO.
    IF AMBIGUOUS tabgco THEN DO:
        X=0.
        IF AVAIL droigco AND droigco.liste_alpha THEN FOR EACH tabgco WHERE tabgco.TYPE_tab = "DS" NO-LOCK  BY tabgco.inti_tab:
            IF tabgco.productif=NO THEN NEXT. /* Avant x=x + 1 pour avoir quand m�me <tous d�p�ts> */
            X=X + 1.
            IF CAN-FIND(FIRST droitdep WHERE droitdep.util=g-user-droit AND droitdep.depot=tabgco.n_tab NO-LOCK) THEN NEXT.
            csoc:ADD-LAST (tabgco.inti_tab + "(" + string(tabgco.n_tab) + ")", "D" + STRING (tabgco.n_tab)).
            ListeDepots=ListeDepots + STRING(tabgco.n_tab) + ",".
        END.
        ELSE FOR EACH tabgco WHERE tabgco.TYPE_tab = "DS" NO-LOCK  BY tabgco.n_tab:
            IF tabgco.productif=NO THEN NEXT. /* Avant x=x + 1 pour avoir quand m�me <tous d�p�ts> */
            X=X + 1.
            IF CAN-FIND(FIRST droitdep WHERE droitdep.util=g-user-droit AND droitdep.depot=tabgco.n_tab NO-LOCK) THEN NEXT.
            csoc:ADD-LAST (tabgco.inti_tab + "(" + string(tabgco.n_tab) + ")", "D" + STRING (tabgco.n_tab)).
            ListeDepots=ListeDepots + STRING(tabgco.n_tab) + ",".
        END.

        IF csoc:NUM-ITEMS>1 THEN DO:
            IF csoc:NUM-ITEMS<>X THEN ListeDepots=TRIM(ListeDepots,",").
            ELSE ListeDepots="".
            csoc:ADD-FIRST("<" + Traduction("Tous d�p�ts autoris�s",-2,"") + ">", "0").
        END.
    END.
    ELSE csoc:ADD-LAST ("<" + Traduction("Tous d�p�ts",-2,"") + ">", "D" + (IF AVAIL tabgco THEN STRING(tabgco.n_tab) ELSE "1")).

    csoc:SCREEN-VAL = csoc:ENTRY(1).

    IF csoc:NUM-ITEMS=1 THEN ASSIGN
        csoc:HIDDEN=YES
        un-depot=YES.
    ELSE IF {2} <> "" AND LOOKUP({2},csoc:LIST-ITEM-PAIRS)>0 THEN 
        csoc:SCREEN-VAL = {2}.
    ELSE IF AVAIL droigco AND droigco.depot>0 AND LOOKUP(STRING(droigco.depot),csoc:LIST-ITEM-PAIRS)>0 THEN 
        csoc:SCREEN-VAL=STRING(droigco.depot).
    ELSE IF LOOKUP(vdepot,csoc:LIST-ITEM-PAIRS)>0 THEN 
        csoc:SCREEN-VAL=vdepot.
END.
