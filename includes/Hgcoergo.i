/*
Auteur    : SC
Date      : 09/06
$A58552 BLI 22/02/13 Ecran de type tactile - saisie BPC
$A100934 CSA 09/04/15 Probl�me de chemin GCOMEDIA quand on est dans Pocwin et que la release de Pocwin est diff�rente de celle de Progiwin
                     (Probl�me rencontr� dans les composants du menu comme ceux de GRC)
*/

DEFINE VARIABLE HndGcoErg AS HANDLE     NO-UNDO.

DEFINE VARIABLE GCOMedia AS CHARACTER   NO-UNDO.
DEFINE VARIABLE StdMedia AS CHARACTER   NO-UNDO.

/* $A100934... */
{g-hprowinbase.i}

IF G-hProwin :Get-Signature("GetAppliRep") = "" THEN 
     GCOMedia = GetSessionData("Config!", "UAPP") + "~\" + GetSessionDataEx("Contexte!", "Progiwin", "Progiwin") + "~\Media".
ELSE GCOMedia = GetAppliRep(GetSessionDataEx("Contexte!", "Progiwin", "Progiwin")) + "~\Media".
/* ...$A100934 */

ASSIGN /* GCOMedia = REPLACE (GetSessionData("Config!", "Media":U),"pocwin","progiwin")*/ /* $A100934 */
                     /*GetSessionData("Config!","Media")*/
                     /*GetSessionData("Config!", "UApp") + "\" + GetSecureVal(GetSessionData("Contexte!", "PROGIWIN"), "PROGIWIN") + "\Media"*/
       StdMedia = GetSessionData("Config!","StdMedia").

/* Pour le Resize */
FUNCTION DO-RESIZELOAD      RETURN LOG IN HndGcoErg.
FUNCTION ADD-RESIZEWIDGET   RETURN LOG (INPUT pfHnd AS WIDGET-HANDLE, INPUT pfStrRes AS CHAR) in HndGcoErg.
FUNCTION ADD-RESIZEWIDGET-P RETURN LOG (INPUT pfHnd AS WIDGET-HANDLE, INPUT pfStrRes AS CHAR, INPUT pfPourcX AS INT, INPUT pfPourcY AS INT) in HndGcoErg.
FUNCTION ADD-RESIZEBWDYN    RETURN LOG (INPUT pfHnd AS WIDGET-HANDLE, INPUT pfStrRes AS CHAR) in HndGcoErg.
FUNCTION ADD-RESIZEBWDYN-P  RETURN LOG (INPUT pfHnd AS WIDGET-HANDLE, INPUT pfStrRes AS CHAR, INPUT pfPourcX AS INT, INPUT pfPourcY AS INT) in HndGcoErg.
FUNCTION DEL-RESIZEWIDGET   RETURN LOG (INPUT pfHnd AS WIDGET-HANDLE) in HndGcoErg.
FUNCTION SET-RESIZEVALUE    RETURN LOG (INPUT pfVar AS CHAR, INPUT pfVal AS CHAR) in HndGcoErg.
FUNCTION SET-RESIZECUM      RETURN LOG (INPUT pfHnd AS WIDGET-HANDLE, INPUT pfCum AS LOG) in HndGcoErg.
FUNCTION SET-RESIZEPOURC    RETURN LOG (INPUT pfHnd AS WIDGET-HANDLE, INPUT pfPourcX AS INT, INPUT pfPourcY AS INT, INPUT pfPourcW AS INT, INPUT pfPourcH AS INT) in HndGcoErg.
FUNCTION DO-RESIZEWINDOW    RETURN LOG IN HndGcoErg.
FUNCTION DO-RESIZEWIDGET    RETURN LOG (INPUT pfHnd AS WIDGET-HANDLE) in HndGcoErg.
FUNCTION DO-RESIZESUPP RETURN LOG (INPUT pfW-P AS INT, INPUT pfH-P AS INT) in HndGcoErg.
FUNCTION SET-MAXISIZE RETURN LOG in HndGcoErg.

/* Pour le Titre */
FUNCTION SET-TITLE RETURN LOG (INPUT PFrameHnd AS HANDLE, INPUT pfTitle AS CHAR) in HndGcoErg.
FUNCTION MAJ-TITLE RETURN LOG (INPUT PFrameHnd AS HANDLE, INPUT pfTitle AS CHAR) in HndGcoErg.
FUNCTION SET-TITLE-Z RETURN LOG (INPUT PFrameHnd AS HANDLE, INPUT pfTitle AS CHAR, INPUT pHeight AS DEC, INPUT pWidth AS DEC, INPUT pPolice AS INT) in HndGcoErg. /*$A58552*/
