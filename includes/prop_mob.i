/* A92523 YO 19/11/14 Recherche du sp�cif par rapport au dossier */
DEFINE VARIABLE PropMOB AS CHARACTER  NO-UNDO.

ASSIGN PropMOB = GetSessionData("Config!", "WMS":U).
       
/*Chemin d'un programme WMS*/
FUNCTION MOB_CHEMINPGM RETURNS CHAR (INPUT pnompgm AS CHAR) :
    DEFINE VARIABLE PgmSpec AS CHARACTER  NO-UNDO.

    /* Recherche si en D�v */
    IF GetSessionData("Session":U,"Mode":U) = "DEV":U THEN PgmSpec = SEARCH("WMS/":U + pnompgm + ".r").
    
    /* A92523 ... */
    /* recherche dans le sous-r�pertoire sp�cifique */
    IF pgmspec = ? OR pgmspec = "" THEN PgmSpec = SEARCH(getsessiondata("config!","rep_spe_dos") + "/WMS/":U + pnompgm).
    ELSE RETURN PgmSpec.
    /* ... A92523 */

    /* Recherche dans le repertoire sp�cif du client */
    IF pgmspec = ? OR pgmspec = "" THEN PgmSpec = SEARCH(getsessiondata("config!","rep_spe") + "/WMS/":U + pnompgm + ".r").
    ELSE RETURN PgmSpec.
    
    /* Recherche dans le repertoire standard */
    IF pgmSpec = ? OR pgmspec = "" THEN PgmSpec = SEARCH(PropMOB + "/" + pnompgm + ".r").
    ELSE RETURN PgmSpec.

    RETURN PgmSpec.

END FUNCTION.


FUNCTION MOB_CHEMINIMG RETURNS CHAR (INPUT pnompgm AS CHAR) :
    DEFINE VARIABLE PgmSpec AS CHARACTER  NO-UNDO.

    /* Recherche si en D�v */
    IF GetSessionData("Session":U,"Mode":U) = "DEV":U THEN PgmSpec = SEARCH("WMS/":U + pnompgm).

    /* recherche dans le sous-r�pertoire sp�cifique */
    IF pgmspec = ? OR pgmspec = "" THEN PgmSpec = SEARCH(getsessiondata("config!","rep_spe_dos") + "/WMS/":U + pnompgm).
    ELSE RETURN PgmSpec.
    
    /* Recherche dans le repertoire sp�cif du client */
    IF pgmspec = ? OR pgmspec = "" THEN PgmSpec = SEARCH(getsessiondata("config!","rep_spe") + "/WMS/":U + pnompgm).
    ELSE RETURN PgmSpec.
    
    /* Recherche dans le repertoire standard */
    IF pgmSpec = ? OR pgmspec = "" THEN PgmSpec = SEARCH(PropMOB + "/" + pnompgm).
    ELSE RETURN PgmSpec.

    RETURN PgmSpec.

END FUNCTION.
