/*  Auteur : HL 
Droit intervenant li� au client 
Dans proc�dure initiale apr�s g-user-droit

09/04/10 CGU Modifications pour base des temps
*/

/*$2104...*/
DEFINE VARIABLE buf-pers-cli AS HANDLE      NO-UNDO.
CREATE BUFFER buf-pers-cli FOR TABLE "tppersonne".

buf-pers-cli:FIND-FIRST("where tppersonne.cod_pers = " + QUOTER(g-user), NO-LOCK) NO-ERROR.
IF NOT buf-pers-cli:AVAILABLE THEN
    buf-pers-cli:FIND-FIRST("where tppersonne.login = " + QUOTER(g-user), NO-LOCK) NO-ERROR.

IF buf-pers-cli:AVAILABLE THEN DO:
    user-int=buf-pers-cli::cod_pers.
    IF buf-pers-cli::fct_com THEN 
        pas-droit-com=can-find(droigco where droigco.util=g-user-droit and droigco.fonc="CLICOM").
    ELSE IF buf-pers-cli::fct_tlv THEN 
        pas-droit-tlv=can-find(droigco where droigco.util=g-user-droit and droigco.fonc="CLITLV").
    ASSIGN
        pas-droit-rpa=can-find(droigco where droigco.util=g-user-droit and droigco.fonc="CLIINT")
        pas-droit-pri=can-find(droigco where droigco.util=g-user-droit and droigco.fonc="CLIPRI")
        pas-droit-res=can-find(droigco where droigco.util=g-user-droit and droigco.fonc="CLIRES")
        pas-droit-aux=can-find(droigco where droigco.util=g-user-droit and droigco.fonc="CLIAUX").
END.
DELETE OBJECT buf-pers-cli NO-ERROR.
/*...$2104*/
