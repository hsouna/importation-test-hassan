/* Auteur : HL */
/********************************************************************************
      !       ATTENTION LES ELEMENTS SUIVANTS POINTENT SUR DES TABLES TEMPORAIRES
     ! !         - focondi
    !   !        - clcondi
   !  !  !
  !   !   !
 !    !    !
!!!!!!!!!!!!  SI UTILISER A PARTIR EDTCOUV2.P
*********************************************************************************/
DEF SHARED VARIABLE g-nda   AS INT. /* Nb décimales prix achat */
DEF VAR fmta AS CHAR no-undo.
fmta=(IF g-nda = 0 THEN "9" ELSE "9." + FILL("9",g-nda)).
