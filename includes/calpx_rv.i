/*INCLUDE:calpx_rv.i...*/
/* 
    {1} = parsoc.typ_rem (gestion des remises en valeur Oui/Non)
    {2} = variable prix � modifier
    {3} = variable pour l'arrondi arrondi
    {4}...{7} = variables des remises 1..4
    {8}...{11} = variables des remises en valeur 1..4
    
    NB : ces param�tres sont dans le m�me ordres que l'appel � calpx-remval de l'api remval.p
 */

if {1} and {8} then {2} = {2} - {4}.
else {2} = {2} - (({2} * {4}) / 100).
if {1} and {9} then {2} = {2} - {5}.
else {2} = {2} - (({2} * {5}) / 100).
if {1} and {10} then {2} = {2} - {6}.
else {2} = {2} - (({2} * {6}) / 100).
if {1} and {11} then {2} = {2} - {7}.
else {2} = {2} - (({2} * {7}) / 100).

&IF "{3}" <> "NOARR" &THEN
if {3} <> ? then {2} = round({2},{3}).
&ENDIF
/*...INCLUDE:calpx_rv.i*/
