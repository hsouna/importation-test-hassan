/* Auteur : HL
   Date   : 24/04/13
   Source : Api-UL.i 
*/
{g-hprowin.def}

&IF DEFINED(hApi-UL) = 0 &THEN
&GLOBAL-DEFINE hApi-UL hApi-UL
DEF VAR hApi-UL AS HANDLE NO-UNDO.
DEF VAR hApi-UL### AS C NO-UNDO.
{g-hprowinbase.i}
hApi-UL### = GetSessionData ("hApi-UL":U,"CLE":U).
hApi-UL = WIDGET-HANDLE(hApi-UL###) NO-ERROR.

IF NOT VALID-HANDLE (hApi-UL) THEN DO:
    RUN api-ul PERSISTENT SET hApi-UL.
    SetSessionData ("hApi-UL":U,"CLE":U,STRING(hApi-UL)).
END.
&ENDIF

/* Calcule le nombre d'unit�s �l�mentaires (U.E) en fonction de l'unit� d'achat, vente, stock, ... (U.L) */
FUNCTION UL_NbUE RETURN DEC (INPUT P%Qte  AS DEC, INPUT P%Uni_ AS CHAR, INPUT P%Nb_1 AS DEC, INPUT P%Nb_2 AS DEC, INPUT P%Nb_u AS DEC, INPUT P%Nb_3 AS DEC, INPUT P%Nb_c AS DEC) in hApi-UL.

/* Calcule le nombre d'unit�s d'achat, vente, stock, ... (U.L) en fonction des unit�s �l�mentaires (U.E) */
FUNCTION UL_NbUL RETURN DEC (INPUT P%Qte  AS DEC, INPUT P%Uni_ AS CHAR, INPUT P%Nb_1 AS DEC, INPUT P%Nb_2 AS DEC, INPUT P%Nb_u AS DEC, INPUT P%Nb_3 AS DEC, INPUT P%Nb_c AS DEC, INPUT P%Dec AS INT) in hApi-UL.

/* Convertit une quantit� d'une unit� X vers une unit� Y*/
FUNCTION UL_NbUXToUY RETURN DEC (INPUT P%Qte  AS DEC, INPUT P%UniX_ AS CHAR, INPUT P%NbX_1 AS DEC, INPUT P%NbX_2 AS DEC, INPUT P%NbX_u AS DEC, INPUT P%NbX_3 AS DEC, INPUT P%NbX_c AS DEC,
                                                      INPUT P%UniY_ AS CHAR, INPUT P%NbY_1 AS DEC, INPUT P%NbY_2 AS DEC, INPUT P%NbY_u AS DEC, INPUT P%NbY_3 AS DEC, INPUT P%NbY_c AS DEC, INPUT P%Dec AS INT) in hApi-UL.

/* Renvoi le code de l'unit� logistique (U.L) */
FUNCTION UL_CodeUL RETURN CHAR (INPUT P%Uni_ AS CHAR, INPUT P%Lib_u AS CHAR, INPUT P%Lib_1 AS CHAR, INPUT P%Lib_2 AS CHAR, INPUT P%Lib_c AS CHAR, INPUT P%Lib_3 AS CHAR, INPUT P%Lib_s AS CHAR) in hApi-UL.

/* Renvoi le d�tail de l'U.L choisie (U.L) */
FUNCTION UL_DetailUL RETURN CHAR (INPUT P%Uni_ AS CHAR, INPUT P%Lib_u AS CHAR, INPUT P%Lib_1 AS CHAR, INPUT P%Nb_1 AS DEC, INPUT P%Lib_2 AS CHAR, INPUT P%Nb_2 AS DEC, INPUT P%Lib_c AS CHAR, INPUT P%Nb_u AS DEC, INPUT P%Lib_3 AS CHAR, INPUT P%Nb_3 AS DEC, INPUT P%Lib_s AS CHAR, INPUT P%Nb_c AS DEC) in hApi-UL.

/* Renvoi la liste des U.L pour la mettre dans une combo-box */
FUNCTION UL_ListeUL RETURN CHAR (INPUT P%Lib_u AS CHAR, INPUT P%Lib_1 AS CHAR, INPUT P%Nb_1 AS DEC, INPUT P%Lib_2 AS CHAR, INPUT P%Nb_2 AS DEC, INPUT P%Lib_c AS CHAR, INPUT P%Nb_u AS DEC, INPUT P%Lib_3 AS CHAR, INPUT P%Nb_3 AS DEC, INPUT P%Lib_s AS CHAR, INPUT P%Nb_c AS DEC) in hApi-UL.

/* Calcule le nombre d'unit�s d'achat (U.A) � r�approvisionner en fonction des unit�s �l�mentaires de vente demand�es (U.E) */
FUNCTION UL_NbUAReap RETURN DEC (INPUT P%Qte  AS DEC, INPUT P%Uni_ AS CHAR, INPUT P%Nb_1 AS DEC, INPUT P%Nb_2 AS DEC, INPUT P%Nb_u AS DEC, INPUT P%Nb_3 AS DEC, INPUT P%Nb_c AS DEC) in hApi-UL.

/* Calcule le poids brut d'une unit� �lementaire (U.E) */
FUNCTION UL_PoidBrutUE RETURN DEC (INPUT P%Uni_ AS CHAR, INPUT P%Depot AS INT, INPUT P%Dec1 AS CHAR, INPUT P%Dec2 AS CHAR, INPUT P%Dec3 AS CHAR, INPUT P%Dec4 AS CHAR, INPUT P%Dec5 AS CHAR, INPUT P%KVar AS CHAR, INPUT p%RowidP AS ROWID, INPUT p%RowidC AS ROWID, INPUT p%RowidF AS ROWID) in hApi-UL.

/* Renvoi la liste des U.L pour la mettre dans une combo-box en maintenance : mulfou_b */
FUNCTION UL_ListeUL_Mnt RETURN CHAR (INPUT P%creation AS LOG, INPUT P%Lib_u AS CHAR, INPUT P%Lib_1 AS CHAR, INPUT P%Nb_1 AS DEC, INPUT P%Lib_2 AS CHAR, INPUT P%Nb_2 AS DEC, INPUT P%Lib_c AS CHAR, INPUT P%Nb_u AS DEC, INPUT P%Lib_3 AS CHAR, INPUT P%Nb_3 AS DEC, INPUT P%Lib_s AS CHAR, INPUT P%Nb_c AS DEC) in hApi-UL.
