/* Auteur : HL */
DEF SHARED VARIABLE g-nbqte as int. /* Nb décimales qte */
def var fmtq as char no-undo.
fmtq=(IF g-nbqte=0 THEN "9" ELSE "9." + FILL("9",g-nbqte)).
