/*  Auteur : JL 
Droit acheteur li� au fournisseur
Dans proc�dure initiale apr�s g-user-droit
09/04/10 CGU Modifications pour base des temps
*/

/* Fonction acheteur */

/*$2104...*/
DEFINE VARIABLE buf-pers-fou AS HANDLE      NO-UNDO.
CREATE BUFFER buf-pers-fou FOR TABLE "tppersonne".

user-ach="".

buf-pers-fou:FIND-FIRST("where tppersonne.cod_pers = " + QUOTER(g-user), NO-LOCK) NO-ERROR.
IF NOT buf-pers-fou:AVAILABLE THEN
    buf-pers-fou:FIND-FIRST("where tppersonne.login = " + QUOTER(g-user), NO-LOCK) NO-ERROR.

IF buf-pers-fou:AVAILABLE
    THEN ASSIGN user-ach=buf-pers-fou::cod_pers
                pas-droit-ach=can-find(droigco where droigco.util=g-user-droit and droigco.fonc="ACHFOU").
DELETE OBJECT buf-pers-fou.
/*...$2104*/
