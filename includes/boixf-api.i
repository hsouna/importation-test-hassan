/**************************************************************************
                 Application Programming Interface
                          For Logging           
**************************************************************************/

/**************************************************************************
  File: boixf-api.i
  Description: Libraire de fonctions et procédures d'exploitation forestière 
  Author: ten
  Created: 16/09/09
***************************************************************************/

&IF DEFINED(G-hBoiXF) = 0 &THEN
&GLOBAL-DEFINE G-hBoiXF G-hBoiXF
DEF VAR G-hBoiXF AS HANDLE NO-UNDO.
DEF VAR G-hBoiXF### AS C NO-UNDO.
G-hBoiXF### = GetSessionData ("BOISXF":U,"CLE":U).
IF G-hBoiXF### <> "NO" THEN G-hBoiXF = WIDGET-HANDLE(G-hBoiXF###) NO-ERROR.


FUNCTION XF_LibZon  RETURNS CHARACTER (pvar AS CHARACTER) IN G-hBoiXF.
/*FUNCTION GET_NOMZONE     RETURNS CHARACTER (pvar AS CHAR,ptype AS CHAR) IN G-hBoiS-XF.*/
FUNCTION XF_Libelle-Lot RETURNS CHARACTER (kk   AS CHARACTER) IN G-hBoiXF.
FUNCTION XF_EXISTEPARAMETRAGE RETURNS LOG (pdepot AS INT,OUTPUT ptypgsd AS CHAR,OUTPUT pcodgsd AS INT) IN G-hBoiXF.
FUNCTION XF_ValiderFeuille RETURNS LOG (prowid AS ROWID) IN G-hBoiXF.
FUNCTION XF_MajVarStock RETURNS LOG (prowid AS ROWID) IN G-hBoiXF.
FUNCTION XF_Annulersortie RETURNS LOGICAL (INPUT irid_col AS ROWID) IN G-hBoiXF.
FUNCTION XF_MajVarLig RETURNS CHAR (ptypgsd AS CHAR,pcodgsd AS INT,pdepot AS INT,ptypPha AS CHAR,pNum_lot AS CHAR,pNumOri AS CHAR,pTypVar AS CHAR,pVal AS CHAR,psens AS INT,prowid AS ROWID,pno_feu AS CHAR) IN G-hBoiXF.
FUNCTION XF_SupprimerLigne RETURNS LOG (prowid AS ROWID) IN G-hBoiXF.
FUNCTION XF_maj_zones_fixes RETURNS LOG (ptypgsd AS char,pcodgsd AS INT,ptypPha AS CHAR) IN G-hBoiXF.
FUNCTION XF_GetLibFR RETURNS CHAR IN G-hBoiXF.
FUNCTION XF_VerifLigInv RETURN LOG (input iBuf AS HANDLe,output bloque AS LOG) IN G-hBoiXF.
FUNCTION XF_devaliderFeuille RETURN LOG (INPUT iNo_feu AS INT,INPUT iMsg AS LOG) IN G-hBoiXF.

IF G-hBoiXF### <> "NO" AND NOT VALID-HANDLE (G-hBoiXF) THEN DO:
    IF NOT AVAILABLE(parsoc) THEN FIND FIRST parsoc NO-LOCK NO-ERROR.
    IF AVAILABLE parsoc AND parsoc.bois_ar THEN DO:
        RUN boixf-api PERSISTENT SET G-hBoiXF.
        SetSessionData ("BOISXF":U,"CLE":U,STRING(G-hBoiXF)).
    END.
    ELSE SetSessionData ("BOISXF":U,"CLE":U,"NO").
END.

&ENDIF

