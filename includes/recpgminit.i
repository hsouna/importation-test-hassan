/*------------------------------------------------------------------------

  File: recpgminit
  Description: construction du r�pertoire des programmes d'initialisation de VL
  Author: CGU
  
$A61779 CGU 05/06/13 Gestion d'une unit� sp�cifique
------------------------------------------------------------------------*/

DEFINE VARIABLE liste-unit AS CHARACTER NO-UNDO EXTENT 5.
ASSIGN
     liste-unit[1] = "UEXP"
     liste-unit[2] = "UAPP"
     liste-unit[3] = "UDOC"
     liste-unit[4] = ""
     liste-unit[5] = "Rep_Spe".

FUNCTION TrouveRepertoire RETURN CHAR (INPUT pno_pgm AS INT):

    DEFINE VARIABLE vcharlocal AS CHARACTER  NO-UNDO.
    
    find pgm where pgm.no_pgm=pno_pgm no-lock no-error.
    
    IF AVAIL pgm THEN DO:
        IF CAN-DO("1,2,3,5",SUBSTR(pgm.chemin,1,1)) THEN vcharlocal = GetSessionData ("Config!", liste-unit[INT(SUBSTR(pgm.chemin,1,1))]) + SUBSTR(pgm.chemin,2).
        ELSE vcharlocal = pgm.chemin.
        IF SUBSTR(STRING(vcharlocal),LENGTH(vcharlocal),1) <> "/"
        THEN vcharlocal = STRING(vcharlocal) + "/" + TRIM(pgm.pgm) + ".r".
        ELSE vcharlocal = STRING(vcharlocal) + TRIM(pgm.pgm) + ".r".
        IF SEARCH(vcharlocal) = ? THEN vcharlocal = REPLACE(vcharlocal,".r",".p").
        IF SEARCH(vcharlocal) = ? THEN vcharlocal = REPLACE(vcharlocal,".p",".w").
    END.

    RETURN vcharlocal.

END FUNCTION.
