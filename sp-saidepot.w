&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 
  Description: 
  Author: 
  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */


/* Local Variable Definitions ---                                       */
DEF VAR ii AS INT NO-UNDO.
DEF VAR logbid AS LOG NO-UNDO.

{hcombars.i}
{g-hprowinbase.i}

DEF VAR hcombars-f1 AS INT NO-UNDO.
DEF VAR FrmComBars-f1 AS HANDLE.

&SCOPED-DEFINE babandonner 101
&SCOPED-DEFINE bvalider 102

{Hgcoergo.i}
{hfileopen.i}



    /* Parameters Definitions ---                                           */
DEFINE INPUT  PARAMETER pdepot  AS INTEGER    NO-UNDO.
DEFINE INPUT  PARAMETER ptype   AS CHARACTER    NO-UNDO.

    /* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS b$pays RECT-320 RECT-362 
&Scoped-Define DISPLAYED-OBJECTS f$depot ft$titre1 f$nom f$adresse1 ~
f$adresse2 f$adresse3 f$ville f$k_post f$pays f$npays 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON b$pays 
     IMAGE-UP FILE "StdMedia/redo.bmp":U
     IMAGE-DOWN FILE "StdMedia/redo.bmp":U
     IMAGE-INSENSITIVE FILE "StdMedia/redo.bmp":U
     LABEL "" 
     SIZE 3 BY .75.

DEFINE VARIABLE f$adresse1 AS CHARACTER FORMAT "X(38)":U 
     LABEL "Adresse 1" 
     VIEW-AS FILL-IN 
     SIZE 24.86 BY .79 NO-UNDO.

DEFINE VARIABLE f$adresse2 AS CHARACTER FORMAT "X(38)":U 
     LABEL "Adresse 2" 
     VIEW-AS FILL-IN 
     SIZE 24.86 BY .79 NO-UNDO.

DEFINE VARIABLE f$adresse3 AS CHARACTER FORMAT "X(38)":U 
     LABEL "Adresse 3" 
     VIEW-AS FILL-IN 
     SIZE 24.86 BY .79 NO-UNDO.

DEFINE VARIABLE f$depot AS INTEGER FORMAT ">>>>>9":U INITIAL 0 
     LABEL "N� D�pot" 
     VIEW-AS FILL-IN 
     SIZE 24.86 BY .79 NO-UNDO.

DEFINE VARIABLE f$k_post AS INTEGER FORMAT ">>>>>>9":U INITIAL 0 
     LABEL "Code postal" 
     VIEW-AS FILL-IN 
     SIZE 24.86 BY .79 NO-UNDO.

DEFINE VARIABLE f$nom AS CHARACTER FORMAT "X(25)":U 
     LABEL "Nom D�pot" 
     VIEW-AS FILL-IN 
     SIZE 24.86 BY .79 NO-UNDO.

DEFINE VARIABLE f$npays AS CHARACTER FORMAT "X(255)":U 
     VIEW-AS FILL-IN 
     SIZE 16.57 BY .79
     BGCOLOR 17 FGCOLOR 16  NO-UNDO.

DEFINE VARIABLE f$pays AS CHARACTER FORMAT "X(3)":U 
     LABEL "Pays" 
     VIEW-AS FILL-IN 
     SIZE 5 BY .79 NO-UNDO.

DEFINE VARIABLE f$ville AS CHARACTER FORMAT "X(26)":U 
     LABEL "Ville" 
     VIEW-AS FILL-IN 
     SIZE 24.86 BY .79 NO-UNDO.

DEFINE VARIABLE ft$titre1 AS CHARACTER FORMAT "X(50)":U INITIAL "Gestion des d�pots" 
      VIEW-AS TEXT 
     SIZE 25.29 BY .67
     FGCOLOR 17 FONT 36 NO-UNDO.

DEFINE RECTANGLE RECT-320
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 91 BY 8.

DEFINE RECTANGLE RECT-362
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 27.14 BY 1.08.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     b$pays AT ROW 8.75 COL 59 WIDGET-ID 94
     f$depot AT ROW 4.25 COL 13 COLON-ALIGNED WIDGET-ID 78
     ft$titre1 AT ROW 2.17 COL 1.72 NO-LABEL WIDGET-ID 8
     f$nom AT ROW 5.79 COL 13 COLON-ALIGNED WIDGET-ID 80
     f$adresse1 AT ROW 4.25 COL 52 COLON-ALIGNED WIDGET-ID 82
     f$adresse2 AT ROW 5.88 COL 52 COLON-ALIGNED WIDGET-ID 84
     f$adresse3 AT ROW 7.58 COL 52 COLON-ALIGNED WIDGET-ID 86
     f$ville AT ROW 7.25 COL 13 COLON-ALIGNED WIDGET-ID 88
     f$k_post AT ROW 8.75 COL 13 COLON-ALIGNED WIDGET-ID 90
     f$pays AT ROW 8.75 COL 52 COLON-ALIGNED WIDGET-ID 92
     f$npays AT ROW 8.75 COL 60.43 COLON-ALIGNED NO-LABEL WIDGET-ID 96
     RECT-320 AT ROW 2.5 COL 1
     RECT-362 AT ROW 1.04 COL 1.14 WIDGET-ID 2
    WITH 1 DOWN NO-BOX OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 157.14 BY 17.33
         FONT 49 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Gestion des d�pots"
         COLUMN             = 15.29
         ROW                = 10.63
         HEIGHT             = 9.38
         WIDTH              = 92.14
         MAX-HEIGHT         = 320
         MAX-WIDTH          = 320
         VIRTUAL-HEIGHT     = 320
         VIRTUAL-WIDTH      = 320
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

&IF '{&WINDOW-SYSTEM}' NE 'TTY' &THEN
IF NOT C-Win:LOAD-ICON("progiwin.ico":U) THEN
    MESSAGE "Unable to load icon: progiwin.ico"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK.
&ENDIF
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* SETTINGS FOR FILL-IN f$adresse1 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$adresse2 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$adresse3 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$depot IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$k_post IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$nom IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$npays IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$pays IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$ville IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN ft$titre1 IN FRAME DEFAULT-FRAME
   NO-ENABLE ALIGN-L                                                    */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME DEFAULT-FRAME
/* Query rebuild information for FRAME DEFAULT-FRAME
     _Options          = "SHARE-LOCK KEEP-EMPTY"
     _Query            is NOT OPENED
*/  /* FRAME DEFAULT-FRAME */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Gestion des d�pots */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  RUN choose-babandonner.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME b$pays
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b$pays C-Win
ON CHOOSE OF b$pays IN FRAME DEFAULT-FRAME
DO:
  DEFINE VARIABLE cod AS CHARACTER  NO-UNDO.
  DEFINE VARIABLE rid AS ROWID      NO-UNDO.
  RUN dynrechex("recherche pays", /*Nom fenetre*/
                "POCWIN.tabcomp", /*la table*/
                "tabcomp.type_tab = 'PA'", /*Where*/
                "a_tab�Code Pays|inti_tab�Intitule Pays", /*colonnnes affich�s*/
                "a_tab", /*Champs a recuperer*/
                OUTPUT cod, OUTPUT rid,
                "PLEINECRAN", "", ? ).
  f$pays:SCREEN-VALUE = cod.
  APPLY "VALUE-CHANGED" TO f$pays.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME f$pays
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f$pays C-Win
ON VALUE-CHANGED OF f$pays IN FRAME DEFAULT-FRAME /* Pays */
DO:
    ASSIGN f$pays.
    FIND FIRST POCWIN.tabcomp WHERE tabcomp.type_tab = "PA" AND tabcomp.a_tab = f$pays NO-LOCK NO-ERROR.
    IF AVAIL tabcomp THEN
        f$npays:SCREEN-VALUE = tabcomp.inti_tab.
    ELSE
        f$npays:SCREEN-VALUE = "Inexistant".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win C-Win


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */

/* Gestion des lignes/colonnes des fichiers interface */
ON WINDOW-CLOSE OF C-Win DO:
  RUN choose-babandonner.
  RETURN NO-APPLY.
END.

ON alt-cursor-up OF FRAME default-frame ANYWHERE DO:
    DEF VAR logbid AS LOG.
    logbid = CBFocusItem (hcombars-f1,2,{&babandonner}).
END.


ON CLOSE OF THIS-PROCEDURE DO:
    RUN putini.
    IF VALID-HANDLE(hComBars) THEN APPLY "close" TO hComBars.
    IF VALID-HANDLE (HndGcoErg) THEN APPLY "CLOSE" TO HndGcoErg.

    IF VALID-HANDLE (Hfileopen) THEN APPLY "close" TO hFileOpen.
    IF VALID-HANDLE(C-Win) THEN DELETE WIDGET C-Win.
    IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END.
  
ON RETURN OF c-win ANYWHERE DO:
    IF CAN-DO("fill-in,toggle-box,radio-set,combo-box",SELF:TYPE) THEN DO:
      APPLY "tab" TO SELF.
      RETURN NO-APPLY.
    END.
    ELSE IF SELF:TYPE="editor" THEN LOGBID=SELF:INSERT-STRING("~n").
    ELSE IF SELF:TYPE="button" THEN APPLY "choose" TO SELF.
    ELSE IF CAN-DO("browse,selection-list",SELF:TYPE) THEN APPLY "default-action" TO SELF.
END.


ON f9 OF FRAME default-frame ANYWHERE
    RUN choose-bvalider.


ON LEAVE OF c-win ANYWHERE DO:
    IF SELF:bgcolor = 31 THEN SELF:bgcolor = 16.
END.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN proc-init.
  SESSION:SET-WAIT-STATE ("").
  VIEW c-win.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-babandonner C-Win 
PROCEDURE choose-babandonner :
DO WITH FRAME DEFAULT-FRAME : 

        APPLY "close" TO THIS-PROCEDURE.

    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bvalider C-Win 
PROCEDURE choose-bvalider :
DO WITH FRAME DEFAULT-FRAME : 
    DEFINE VARIABLE rep AS LOGICAL    NO-UNDO.
    MESSAGE "Voulez vous valider ?"
        VIEW-AS ALERT-BOX INFO BUTTONS YES-NO UPDATE rep.
    IF rep THEN
    DO:
        ASSIGN
            f$depot   
            f$nom     
            f$adresse1
            f$adresse2
            f$adresse3
            f$ville   
            f$k_post
            f$pays .

        IF ptype = "C" THEN
        DO:
            CREATE tabgco.
            ASSIGN
                tabgco.n_tab        =  f$depot   
                tabgco.inti_tab     =  f$nom     
                tabgco.adresse[1]   =  f$adresse1
                tabgco.adresse[2]   =  f$adresse2
                tabgco.adresse[3]   =  f$adresse3
                tabgco.ville        =  f$ville   
                tabgco.k_post       =  f$k_post
                tabgco.pays         =  f$pays
                tabgco.type_tab     =  "DS".
        END.
        ELSE 
        DO:
            FIND FIRST tabgco WHERE tabgco.type_tab = "DS" AND tabgco.n_tab = pdepot EXCLUSIVE-LOCK NO-ERROR.
            IF AVAIL tabgco THEN
            DO:
                ASSIGN  
                    tabgco.inti_tab     =  f$nom     
                    tabgco.adresse[1]   =  f$adresse1
                    tabgco.adresse[2]   =  f$adresse2
                    tabgco.adresse[3]   =  f$adresse3
                    tabgco.ville        =  f$ville   
                    tabgco.k_post       =  f$k_post
                    tabgco.pays         =  f$pays.
            END.
        END.
        APPLY "close" TO THIS-PROCEDURE.
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsCreation C-Win 
PROCEDURE CombarsCreation :
RUN gcoErgo PERSISTENT SET HndGcoErg ({&WINDOW-NAME}:HANDLE).
    SET-TITLE (FRAME DEFAULT-FRAME:HANDLE, {&WINDOW-NAME}:TITLE). 
    RUN combars PERSISTENT SET hcombars.
    hcombars-f1 = cbocxadd (FRAME DEFAULT-FRAME:HANDLE, OUTPUT FrmComBars-f1).
    CBSetSize(hcombars-f1, 1.04,  28.26, 1.15,FRAME DEFAULT-FRAME:WIDTH - 27.66).
    CBAddBar(hcombars-f1, 'Barre outils 1', {&xtpBarTop}, YES, NO).
    CBShowMenu(hcombars-f1, NO).
    CBToolBarAccelTips (hcombars-f1, NO).

    CBLoadIcon(hcombars-f1, {&babandonner},StdMedia + "\StdMedia.icl",1). 
    CBLoadIcon(hcombars-f1, {&bvalider},StdMedia + "\StdMedia.icl",116).

    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&babandonner}, Translate ('&Abandonner') , YES).
    CBSetItemStyle(hcombars-f1, 2, {&babandonner}, {&xtpButtonIconAndCaption}).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bvalider}, Translate ('&Valider' + ' (F9)') , NO).
    CBSetItemStyle(hcombars-f1, 2, {&bvalider}, {&xtpButtonIconAndCaption}).
        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsExecute C-Win 
PROCEDURE CombarsExecute :
DEF INPUT PARAM pnumcombar AS INT.
    DEF INPUT PARAM pbutton AS INT.

    IF pnumcombar = hcombars-f1 THEN DO:
        CASE pbutton :
            WHEN {&babandonner} THEN RUN choose-babandonner.
            WHEN {&bvalider} THEN RUN choose-bvalider.
        END CASE.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getini C-Win 
PROCEDURE getini :
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc-init C-Win 
PROCEDURE proc-init :
DO WITH FRAME default-frame:
     ENABLE ALL WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
     DEFINE VARIABLE LAST_NUM AS INTEGER    NO-UNDO.

     DISABLE f$depot f$npays WITH FRAME DEFAULT-FRAME.
        
        RUN getini.

        {&OPEN-QUERY-{&BROWSE-NAME}}

        RUN combarscreation.

        IF ptype = "M" THEN
        DO:
            ft$titre1:SCREEN-VALUE = "Modification d�pot".
            FIND FIRST tabgco WHERE tabgco.type_tab = "DS" AND tabgco.n_tab = pdepot EXCLUSIVE-LOCK NO-ERROR.
            IF AVAIL tabgco THEN
            DO:
                ASSIGN
                    f$depot     = tabgco.n_tab
                    f$nom       = tabgco.inti_tab
                    f$adresse1  = tabgco.adresse[1]
                    f$adresse2  = tabgco.adresse[2]
                    f$adresse3  = tabgco.adresse[3]
                    f$ville     = tabgco.ville
                    f$k_post    = tabgco.k_post.
            END.
        END.
        ELSE
        DO:
             ft$titre1:SCREEN-VALUE = "Cr�ation d�pot".
             FOR LAST tabgco NO-LOCK WHERE tabgco.type_tab = "DS" BY tabgco.n_tab  :
                 LAST_NUM = tabgco.n_tab + 1.
             END.

             f$depot     = LAST_NUM.

        END.

        DISPLAY 
            f$depot   
            f$nom     
            f$adresse1
            f$adresse2
            f$adresse3
            f$ville   
            f$k_post.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE putini C-Win 
PROCEDURE putini :
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

