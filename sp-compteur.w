&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          gco              PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 
  Description: 
  Author: 
  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */


/* Local Variable Definitions ---                                       */
DEF VAR ii AS INT NO-UNDO.
DEF VAR logbid AS LOG NO-UNDO.

{hcombars.i}
{g-hprowinbase.i}

DEF VAR hcombars-f1 AS INT NO-UNDO.
DEF VAR FrmComBars-f1 AS HANDLE.

&SCOPED-DEFINE babandonner 101
&SCOPED-DEFINE bcreer 102
&SCOPED-DEFINE baide 103
&SCOPED-DEFINE bmodifier 104
&SCOPED-DEFINE bsupprimer 105

{Hgcoergo.i}
{hfileopen.i}



    /* Parameters Definitions ---                                           */
/* DEFINE INPUT  PARAMETER pliste  AS CHARACTER    NO-UNDO.  */

    /* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME Bwdepot

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tabgco

/* Definitions for BROWSE Bwdepot                                       */
&Scoped-define FIELDS-IN-QUERY-Bwdepot tabgco.n_tab tabgco.inti_tab tabgco.adresse   
&Scoped-define ENABLED-FIELDS-IN-QUERY-Bwdepot   
&Scoped-define SELF-NAME Bwdepot
&Scoped-define QUERY-STRING-Bwdepot FOR EACH  tabgco
&Scoped-define OPEN-QUERY-Bwdepot OPEN QUERY {&SELF-NAME} FOR EACH  tabgco.
&Scoped-define TABLES-IN-QUERY-Bwdepot tabgco
&Scoped-define FIRST-TABLE-IN-QUERY-Bwdepot tabgco


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-Bwdepot}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS F$t-4 RADIO-SET-2 Bwdepot RADIO-SET-1 f$t-1 ~
RECT-320 RECT-362 
&Scoped-Define DISPLAYED-OBJECTS F$t-4 RADIO-SET-2 RADIO-SET-1 ft$titre1 ~
f$t-1 f$t-2 f$t-3 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE f$t-1 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 8.29 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE f$t-2 AS CHARACTER FORMAT "X(256)":U INITIAL "0" 
     VIEW-AS FILL-IN 
     SIZE 18 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE f$t-3 AS CHARACTER FORMAT "X(256)":U INITIAL "0" 
     VIEW-AS FILL-IN 
     SIZE 24 BY 1
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE F$t-4 AS CHARACTER FORMAT "X(256)":U 
     LABEL "NOM de D�pot  :" 
     VIEW-AS FILL-IN 
     SIZE 18 BY .79 NO-UNDO.



DEFINE VARIABLE RADIO-SET-1 AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "N� de D�pot", 1,
"D�signation", 2,
"Adresse", 3
     SIZE 34 BY 1.25 NO-UNDO.

DEFINE VARIABLE RADIO-SET-2 AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "commence", 1,
"contient", 2,
"termine", 3
     SIZE 33 BY 1.25 NO-UNDO.

DEFINE RECTANGLE RECT-320
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 152 BY 19.5.

DEFINE RECTANGLE RECT-362
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 27.14 BY 1.08.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY Bwdepot FOR 
      tabgco SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE Bwdepot
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS Bwdepot C-Win _FREEFORM
  QUERY Bwdepot DISPLAY
      tabgco.n_tab FORMAT ">>>>>9.99" COLUMN-LABEL "N� D�pot" WIDTH 15
      tabgco.inti_tab FORMAT "X(70)" COLUMN-LABEL "D�signation" WIDTH 30
      tabgco.adresse  FORMAT "X(70)" COLUMN-LABEL "Adresse" WIDTH 9
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 126.14 BY 15.25
         FONT 49 ROW-HEIGHT-CHARS .5 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     F$t-4 AT ROW 4 COL 74 COLON-ALIGNED WIDGET-ID 52
     RADIO-SET-2 AT ROW 3.75 COL 27.43 NO-LABEL WIDGET-ID 48
     Bwdepot AT ROW 5.25 COL 1.86 WIDGET-ID 200
     RADIO-SET-1 AT ROW 2.25 COL 27 NO-LABEL WIDGET-ID 44
     
     f$t-1 AT ROW 2.33 COL 64 COLON-ALIGNED NO-LABEL WIDGET-ID 38
     f$t-2 AT ROW 2.33 COL 74 COLON-ALIGNED NO-LABEL WIDGET-ID 40
     f$t-3 AT ROW 2.29 COL 94 COLON-ALIGNED NO-LABEL WIDGET-ID 42
     RECT-320 AT ROW 2 COL 1
     RECT-362 AT ROW 1.04 COL 1.14 WIDGET-ID 2
    WITH 1 DOWN NO-BOX OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 162.43 BY 23.5
         FONT 49 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "MAJ de fiche D�pot"
         COLUMN             = 15.29
         ROW                = 7
         HEIGHT             = 23.5
         WIDTH              = 166.14
         MAX-HEIGHT         = 320
         MAX-WIDTH          = 320
         VIRTUAL-HEIGHT     = 320
         VIRTUAL-WIDTH      = 320
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

&IF '{&WINDOW-SYSTEM}' NE 'TTY' &THEN
IF NOT C-Win:LOAD-ICON("progiwin.ico":U) THEN
    MESSAGE "Unable to load icon: progiwin.ico"
            VIEW-AS ALERT-BOX WARNING BUTTONS OK.
&ENDIF
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB Bwdepot 1 DEFAULT-FRAME */
/* SETTINGS FOR FILL-IN f$t-2 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$t-3 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN ft$titre1 IN FRAME DEFAULT-FRAME
   NO-ENABLE ALIGN-L                                                    */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE Bwdepot
/* Query rebuild information for BROWSE Bwdepot
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH  tabgco.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE Bwdepot */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME DEFAULT-FRAME
/* Query rebuild information for FRAME DEFAULT-FRAME
     _Options          = "SHARE-LOCK KEEP-EMPTY"
     _Query            is NOT OPENED
*/  /* FRAME DEFAULT-FRAME */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* r�initialisation des compteurs */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  RUN choose-babandonner.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME f$t-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f$t-2 C-Win
ON LEAVE OF f$t-2 IN FRAME DEFAULT-FRAME
DO:
  ASSIGN f$t-2.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME f$t-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL f$t-3 C-Win
ON LEAVE OF f$t-3 IN FRAME DEFAULT-FRAME
DO:
  ASSIGN f$t-3.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME RADIO-SET-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL RADIO-SET-1 C-Win
ON VALUE-CHANGED OF RADIO-SET-1 IN FRAME DEFAULT-FRAME
DO:
    ASSIGN RADIO-SET-1.
                    IF RADIO-SET-1 = 1 THEN DISABLE f$t-2 f$t-3 WITH FRAME DEFAULT-FRAME. 
  
                    IF RADIO-SET-1 = 2 THEN DISABLE f$t-1 f$t-3 WITH FRAME DEFAULT-FRAME.
      
                    IF RADIO-SET-1 = 3 THEN DISABLE f$t-2 f$t-1 WITH FRAME DEFAULT-FRAME. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME Bwdepot
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win C-Win


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
            ASSIGN CURRENT-WINDOW    = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */

/* Gestion des lignes/colonnes des fichiers interface */
ON WINDOW-CLOSE OF C-Win DO:
  RUN choose-babandonner.
  RETURN NO-APPLY.
END.

ON alt-cursor-up OF FRAME default-frame ANYWHERE DO:
    DEF VAR logbid AS LOG.
    logbid = CBFocusItem (hcombars-f1,2,{&babandonner}).
END.


ON CLOSE OF THIS-PROCEDURE DO:
    RUN putini.
    IF VALID-HANDLE(hComBars) THEN APPLY "close" TO hComBars.
    IF VALID-HANDLE (HndGcoErg) THEN APPLY "CLOSE" TO HndGcoErg.

    IF VALID-HANDLE (Hfileopen) THEN APPLY "close" TO hFileOpen.
    IF VALID-HANDLE(C-Win) THEN DELETE WIDGET C-Win.
    IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END.
  
ON RETURN OF c-win ANYWHERE DO:
    IF CAN-DO("fill-in,toggle-box,radio-set,combo-box",SELF:TYPE) THEN DO:
      APPLY "tab" TO SELF.
      RETURN NO-APPLY.
    END.
    ELSE IF SELF:TYPE="editor" THEN LOGBID=SELF:INSERT-STRING("~n").
    ELSE IF SELF:TYPE="button" THEN APPLY "choose" TO SELF.
    ELSE IF CAN-DO("browse,selection-list",SELF:TYPE) THEN APPLY "default-action" TO SELF.
END.


ON f9 OF FRAME default-frame ANYWHERE
    RUN choose-bcreer.

ON LEAVE OF c-win ANYWHERE DO:
    IF SELF:bgcolor = 31 THEN SELF:bgcolor = 16.
END.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN proc-init.
  SESSION:SET-WAIT-STATE ("").
  VIEW c-win.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-babandonner C-Win 
PROCEDURE choose-babandonner :
DO WITH FRAME DEFAULT-FRAME : 

        APPLY "close" TO THIS-PROCEDURE.

    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-baide C-Win 
PROCEDURE choose-baide :
DO WITH FRAME DEFAULT-FRAME : 

    MESSAGE "$999 A FAIRE" VIEW-AS ALERT-BOX INFO BUTTONS OK.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bcreer C-Win 
PROCEDURE choose-bcreer :
DO WITH FRAME DEFAULT-FRAME : 
DEFINE VARIABLE reps AS LOGICAL    NO-UNDO.
    
MESSAGE "VOULEZ VOUS Ajouter un Enregistrement ? "
            VIEW-AS ALERT-BOX INFO BUTTONS YES-NO UPDATE reps.
 IF f$t-1 <> 0  THEN DO:
        
            IF reps = YES THEN
            DO:
            CREATE tabgco.
                FOR EACH tabgco WHERE tabgco.type_tab = "DS" NO-LOCK:
            
            ASSIGN  tabgco.n_tab    = f$t-1
                    tabgco.inti_tab = f$t-2
                    tabgco.adresse  = f$t-3.
            END.
           MESSAGE "LE TRAITEMENT TERMINE AVEC SUCCES !"
                    VIEW-AS ALERT-BOX INFO BUTTONS OK.
         
            END.
RUN proc-init.
    END.

OPEN QUERY Bwdepot FOR EACH tabgco WHERE tabgco.type_tab = "DS"  NO-LOCK.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bsupprimer C-Win 
PROCEDURE choose-bsupprimer :
DO WITH FRAME DEFAULT-FRAME : 
DEFINE VARIABLE rep1 AS LOGICAL    NO-UNDO.
   
        MESSAGE "voulez vous vraiment supprimer"
            VIEW-AS ALERT-BOX INFO BUTTONS YES-NO UPDATE rep1.
      IF rep1 THEN
        DO:
            IF AVAIL tabgco THEN 
            DO:

                FIND CURRENT tabgco EXCLUSIVE-LOCK NO-ERROR.
                DELETE tabgco.
            END.
        END.
          

    OPEN QUERY Bwdepot FOR EACH tabgco WHERE tabgco.type_tab = "DS"  NO-LOCK.
END. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bmodifier C-Win 
PROCEDURE choose-bmodifier :
DO WITH FRAME DEFAULT-FRAME : 
DEFINE VARIABLE reps AS LOGICAL    NO-UNDO.
    
MESSAGE "VOULEZ VOUS Modifier  L'Enregistrement ? "
            VIEW-AS ALERT-BOX INFO BUTTONS YES-NO UPDATE reps.
         
            IF reps = YES THEN
            DO:
                     IF AVAIL tabgco THEN 
                       DO:

                        FIND CURRENT tabgco EXCLUSIVE-LOCK NO-ERROR.
                      
               ASSIGN  tabgco.n_tab    = f$t-1
                       tabgco.inti_tab = f$t-2
                       tabgco.adresse  = f$t-3.
            
                      END.
            END.
          
        
   
OPEN QUERY Bwdepot FOR EACH tabgco WHERE tabgco.type_tab = "DS"  NO-LOCK.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsCreation C-Win 
PROCEDURE CombarsCreation :
RUN gcoErgo PERSISTENT SET HndGcoErg ({&WINDOW-NAME}:HANDLE).
    SET-TITLE (FRAME DEFAULT-FRAME:HANDLE, {&WINDOW-NAME}:TITLE). 
    RUN combars PERSISTENT SET hcombars.
    hcombars-f1 = cbocxadd (FRAME DEFAULT-FRAME:HANDLE, OUTPUT FrmComBars-f1).
    CBSetSize(hcombars-f1, 1.04,  28.26, 1.15,FRAME DEFAULT-FRAME:WIDTH - 27.66).
    CBAddBar(hcombars-f1, 'Barre outils 1', {&xtpBarTop}, YES, NO).
    CBShowMenu(hcombars-f1, NO).
    CBToolBarAccelTips (hcombars-f1, NO).

    CBLoadIcon(hcombars-f1, {&babandonner},StdMedia + "\StdMedia.icl",1).
    CBLoadIcon(hcombars-f1, {&bcreer},StdMedia + "\StdMedia.icl",36).
    CBLoadIcon(hcombars-f1, {&bmodifier},StdMedia + "\StdMedia.icl",78).
    CBLoadIcon(hcombars-f1, {&bsupprimer},StdMedia + "\StdMedia.icl",112).

    
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bcreer}, Translate ('&Creer' + ' (F9)') , NO).
    CBSetItemStyle(hcombars-f1, 2, {&bcreer}, {&xtpButtonIconAndCaption}).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bmodifier}, Translate ('&Modifier' + ' (F9)') , NO).
    CBSetItemStyle(hcombars-f1, 2, {&bmodifier}, {&xtpButtonIconAndCaption}).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bsupprimer}, Translate ('&Supprimer' + ' (F9)') , NO).
    CBSetItemStyle(hcombars-f1, 2, {&bsupprimer}, {&xtpButtonIconAndCaption}).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&babandonner}, Translate ('&Abandonner') , YES).
    CBSetItemStyle(hcombars-f1, 2, {&babandonner}, {&xtpButtonIconAndCaption}).
        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsExecute C-Win 
PROCEDURE CombarsExecute :
DEF INPUT PARAM pnumcombar AS INT.
    DEF INPUT PARAM pbutton AS INT.

    IF pnumcombar = hcombars-f1 THEN DO:
        CASE pbutton :
            WHEN {&babandonner} THEN RUN choose-babandonner.
            WHEN {&bcreer} THEN RUN choose-bcreer.
            WHEN {&bmodifier} THEN RUN choose-bmodifier.
            WHEN {&bsupprimer} THEN RUN choose-bsupprimer.
        END CASE.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getini C-Win 
PROCEDURE getini :
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc-init C-Win 
PROCEDURE proc-init :

  DO WITH FRAME DEFAULT-FRAME :
        ENABLE ALL WITH FRAME DEFAULT-FRAME IN WINDOW C-win.

   
        
        RUN getini.



        RUN combarscreation.
          OPEN QUERY Bwdepot FOR EACH tabgco WHERE tabgco.type_tab = "DS"  NO-LOCK.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE putini C-Win 
PROCEDURE putini :
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

